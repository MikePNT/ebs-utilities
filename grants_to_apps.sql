 -- ============================================================================
 --
 -- MODULE NAME    : _grants_to_apps.sql
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Build XXNT Class objects
 --
 --
 -- Dependencies:
 -- -------------
 -- None
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       14-Feb-2019   M Proctor         Initial version
 -- 1.1       14-Aug-2019   M Proctor         EBSDS-1078 is logging and
 --                                           messaging only.
 --                                           Rem out permissions not required
 --                                           for messaging or logging.
 --
 --
 -- ============================================================================
 --
 --

--PROMPT Grant execute on xx.xxnt_timer_i
--GRANT EXECUTE ON xx.xxnt_timer_i TO apps;

--PROMPT Grant execute on xx.xxnt_formatter_time_i
--GRANT EXECUTE ON xx.xxnt_formatter_time_i TO apps;

--PROMPT Grant execute on xx.xxnt_time_factory_o
--GRANT EXECUTE ON xx.xxnt_time_factory_o TO apps;

--PROMPT Grant execute on xx.xxnt_count_i
--GRANT EXECUTE ON xx.xxnt_count_i TO apps;

--PROMPT Grant execute on xx.xxnt_dml_counter_i
--GRANT EXECUTE ON xx.xxnt_dml_counter_i TO apps;

--PROMPT Grant execute on xx.xxnt_file_counter_i
--GRANT EXECUTE ON xx.xxnt_file_counter_i TO apps;

--PROMPT Grant execute on xx.xxnt_count_factory_o
--GRANT EXECUTE ON xx.xxnt_count_factory_o TO apps;

PROMPT Grant execute on xx.xxnt_message
GRANT EXECUTE ON xx.xxnt_message TO apps;

PROMPT Grant execute on xx.xxnt_logger_i
GRANT EXECUTE ON xx.xxnt_logger_i TO apps;

PROMPT Grant under on xx.xxnt_object_a
GRANT UNDER ON xx.xxnt_object_a TO apps;

PROMPT Grant execute on xx.xxnt_object_a
GRANT EXECUTE ON xx.xxnt_object_a TO apps;

PROMPT Grant execute on xx.xxnt_class_o
GRANT EXECUTE ON xx.xxnt_class_o TO apps;

PROMPT Grant execute on xx.xxnt_logger_factory_o
GRANT EXECUTE ON xx.xxnt_logger_factory_o TO apps;

--PROMPT Grant execute on xx.xxnt_concurrent
--GRANT EXECUTE ON xx.xxnt_concurrent TO apps;

--PROMPT Grant execute on xx.xxnt_file_i
--GRANT EXECUTE ON xx.xxnt_file_i TO apps;

--PROMPT Grant execute on xx.xxnt_file_o
--GRANT EXECUTE ON xx.xxnt_file_o TO apps;

--PROMPT Grant execute on xx.xxnt_directory_i
--GRANT EXECUTE ON xx.xxnt_directory_i TO apps;

--PROMPT Grant execute on xx.xxnt_file_list_t
--GRANT EXECUTE ON xx.xxnt_file_list_t TO apps;

--PROMPT Grant execute on xx.xxnt_file_factory_o
--GRANT EXECUTE ON xx.xxnt_file_factory_o TO apps;

EXIT;

