 -- ============================================================================
 --
 -- MODULE NAME    : _grants_to_xx.sql
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Build XXNT Class objects
 --
 --
 -- Dependencies:
 -- -------------
 -- None
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       14-Feb-2019   M Proctor         Initial version
 -- 1.1       30-Jun-2019   M Proctor         Add permissions on
 --                                           fnd_log_messages
 -- 1.2       14-Aug-2019   M Proctor         EBSDS-1078 is logging and
 --                                           messaging only.
 --                                           Rem out permissions not required
 --                                           for messaging or logging.
 --
 --
 -- ============================================================================
 --
 --

PROMPT Grant execute on apps.fnd_profile
GRANT EXECUTE ON apps.fnd_profile TO xx;

PROMPT Grant execute on apps.fnd_file
GRANT EXECUTE ON apps.fnd_file TO xx;

PROMPT Grant execute on apps.fnd_message
GRANT EXECUTE ON apps.fnd_message TO xx;

PROMPT Grant execute on apps.fnd_msg_pub
GRANT EXECUTE ON apps.fnd_msg_pub TO xx;

PROMPT Grant execute on apps.fnd_log_repository
GRANT EXECUTE ON apps.fnd_log_repository TO xx;

PROMPT Grant execute on apps.fnd_api
GRANT EXECUTE ON apps.fnd_api TO xx;

PROMPT Grant execute on apps.fnd_log
GRANT EXECUTE ON apps.fnd_log TO xx;

PROMPT Grant select on applsys.fnd_concurrent_programs
GRANT SELECT ON applsys.fnd_concurrent_programs TO xx;

PROMPT Grant select on applsys.fnd_concurrent_programs_tl
GRANT SELECT ON applsys.fnd_concurrent_programs_tl TO xx;

PROMPT Grant select on applsys.fnd_concurrent_requests
GRANT SELECT ON applsys.fnd_concurrent_requests TO xx;

PROMPT Grant select on applsys.fnd_profile_option_values
GRANT SELECT ON applsys.fnd_profile_option_values TO xx;

PROMPT Grant select on applsys.fnd_profile_options
GRANT SELECT ON applsys.fnd_profile_options TO xx;

PROMPT Grant insert on applsys.fnd_log_messages
GRANT INSERT ON applsys.fnd_log_messages TO xx;

PROMPT Grant select on applsys.fnd_log_messages
GRANT SELECT ON applsys.fnd_log_messages TO xx;

PROMPT Grant select on apps.fnd_log_messages_s
GRANT SELECT ON apps.fnd_log_messages_s TO xx;

PROMPT Grant select on applsys.fnd_log_transaction_context
GRANT SELECT ON applsys.fnd_log_transaction_context to xx;
 
EXIT;
