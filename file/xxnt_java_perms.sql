 -- ============================================================================
 --
 -- Module Name    : xxnt_java_perms.sql
 -- Original Author: M Proctor
 -- Date           : 17-April-2019
 -- Description    : Grant Java permissions to XX
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       17-Apr-2019   M Proctor         Initial version
 --
 --
 -- ============================================================================


BEGIN
dbms_java.grant_permission('XX','SYS:java.io.FilePermission','/s01/-','read,write,execute,delete');
dbms_java.grant_permission('XX','SYS:java.io.FilePermission','/s01/','read,write,execute,delete');
END;
/

EXIT

