--
set define off
--
CREATE OR REPLACE AND COMPILE JAVA SOURCE NAMED "xxntFile" AS


import java.io.*;
import xxntList.*;

 /*
 -- +==========================================================================+
 -- |                    National Trust
 -- +==========================================================================+
 -- | File Name   : xxntFile.java
 -- | Module      : Utilities
 -- | Author      : M Proctor
 -- | Date        : 17-Apr-2019
 -- | Description : File Utilities
 -- |
 -- | Doc Ref(s)  : 
 -- |
 -- | Change Record:
 -- | ===============
 -- | Ver  Date         Author           Comments
 -- | ===  ===========  =============    ======================================+
 -- | 1.0  17-Apr-2019  M Proctor        Initial Build
 -- | 
 -- | 
 -- +==========================================================================+
*/

public class xxntFile
{
  private static xxntList list = new xxntList();

  private static FileHandle getFileHandle(int fileId) 
  {
    return (FileHandle)list.get(fileId);
  }

  private static File getFile(int fileId) 
  {
    return getFileHandle(fileId).file;
  }
  
  public static int getFileId(String filename) 
  { 
    FileHandle fileHandle = new FileHandle();

    fileHandle.file = new File(filename);
    
    return list.addElement(fileHandle); 
  }

  public static String getSeparator() { return File.separator; }

  public static int canRead(int fileId) { return getFile(fileId).canRead() ? 1 : 0; }

  public static int canWrite(int fileId) { return getFile(fileId).canWrite() ? 1 : 0; }
  
  public static int createNewFile(int fileId) throws IOException { return getFile(fileId).createNewFile() ? 1 : 0; }

  public static int delete(int fileId) { return getFile(fileId).delete() ? 1 : 0; }
  
  public static int exists(int fileId) { return getFile(fileId).exists() ? 1 : 0; }

  public static String getAbsolutePath(int fileId) throws IOException { return getFile(fileId).getAbsolutePath(); }

  public static String getCanonicalPath(int fileId) throws IOException { return getFile(fileId).getCanonicalPath(); }

  public static String getName(int fileId) { return getFile(fileId).getName(); }

  public static String getParent(int fileId) { return getFile(fileId).getParent(); }

  public static String getPath(int fileId) { return getFile(fileId).getPath(); }

  public static int isAbsolute(int fileId) { return getFile(fileId).isAbsolute() ? 1 : 0; }

  public static int isDirectory(int fileId) { return getFile(fileId).isDirectory() ? 1 : 0; }

  public static int isFile(int fileId) { return getFile(fileId).isFile() ? 1 : 0; }

  public static int isHidden(int fileId) { return getFile(fileId).isHidden() ? 1 : 0; }

  public static long length(int fileId) { return getFile(fileId).length(); }
  
  public static int mkdir(int fileId) { return getFile(fileId).mkdir() ? 1 : 0; }

  public static int mkdirs(int fileId) { return getFile(fileId).mkdirs() ? 1 : 0; }

  public static int renameTo(int fileId, String filename) { return getFile(fileId).renameTo(new File(filename)) ? 1 : 0; }

  public static int setReadOnly(int fileId) { return getFile(fileId).setReadOnly() ? 1 : 0; }

  public static void releaseFile(int fileId) { list.removeElement(fileId); }

  public static void openNew(int fileId) throws Exception
  { 
    getFileHandle(fileId).printWriter = new PrintWriter( new BufferedWriter ( new OutputStreamWriter( new FileOutputStream(getFile(fileId)))));
  }    

  public static void openAppend(int fileId) throws Exception
  {
    getFileHandle(fileId).printWriter = new PrintWriter( new BufferedWriter ( new OutputStreamWriter( new FileOutputStream(getFile(fileId)))));
  }

  public static void openRead(int fileId) throws Exception
  { 
    getFileHandle(fileId).bufferedReader = new BufferedReader ( new FileReader(getFile(fileId)));
  }    

  public static void putLine(int fileId, String line) 
  {
    getFileHandle(fileId).printWriter.println(line);
  }

  public static void readLine(int fileId, String[] line, int[] eof) throws Exception
  {
    line[0] = getFileHandle(fileId).bufferedReader.readLine();
    eof[0]  = (line[0] == null ? 1 : 0);    
  }
  
  public static void close(int fileId) throws Exception
  {
    if (getFileHandle(fileId).printWriter != null) 
    {
      getFileHandle(fileId).printWriter.close();
    }

    if (getFileHandle(fileId).bufferedReader != null) 
    {
      getFileHandle(fileId).bufferedReader.close();
    }
  }

  public static int fileCount(int fileId) 
  {
    return getFile(fileId).listFiles(new FileOnlyFilter()).length;
  }

  public static int dirCount(int fileId) 
  {
    return getFile(fileId).listFiles(new DirOnlyFilter()).length;
  }


  public static String getFileName(int fileId, int index) 
  {
    return getFile(fileId).listFiles(new FileOnlyFilter())[index].getName(); 
  }

  public static void copy(int fileId, String destFilename) throws Exception
  {
   BufferedInputStream   input  = new BufferedInputStream(new FileInputStream(getFile(fileId)));  
   BufferedOutputStream  output = new BufferedOutputStream(new FileOutputStream(destFilename)); 

   int b;

   while ((b = input.read()) != -1) { output.write(b); }

   input.close(); 
   output.close(); 
  }

  private static class FileHandle 
  {
    File           file;
    PrintWriter    printWriter;
    BufferedReader bufferedReader;
  }

  private static class FileOnlyFilter implements FileFilter 
  {
    public boolean accept(File pathname) 
    {
      return pathname.isFile();
    }
  }


  private static class DirOnlyFilter implements FileFilter 
  {
    public boolean accept(File pathname) 
    {
      return pathname.isDirectory();
    }
  }


  public static void putDelimitedData(int fileId, String line, String delimiter) 
  {
    print(getFileHandle(fileId).printWriter, line == null ? "" : line, delimiter.charAt(0));
  }


  public static void putDelimitedNewLine(int fileId)
  {
    getFileHandle(fileId).printWriter.println();
		getFileHandle(fileId).printWriter.flush();
		newLine = true;
  }

	/**
	 * True iff we just began a new line.
	 */
	private static boolean newLine = true;


	/**
	 * Print the string as the next value on the line.	The value
	 * will be quoted if needed.
	 *
	 * @param value value to be outputted.
	 */
	private static void print(PrintWriter out, String value, char delimiter){
		boolean quote = false;
		if (value.length() > 0){
			char c = value.charAt(0);
			if (newLine && (c<'0' || (c>'9' && c<'A') || (c>'Z' && c<'a') || (c>'z'))){
				quote = true;
			}
			if (c==' ' || c=='\f' || c=='\t'){
				quote = true;
			}
			for (int i=0; i<value.length(); i++){
				c = value.charAt(i);
				if (c=='"' || c==',' || c=='\n' || c=='\r'){
					quote = true;
				}
			}
			if (c==' ' || c=='\f' || c=='\t'){
				quote = true;
			}
		} else if (newLine) {
			// always quote an empty token that is the firs
			// on the line, as it may be the only thing on the
			// line.  If it were not quoted in that case,
			// an empty line has no tokens.
			quote = true;
		}
		if (newLine){
			newLine = false;
		} else {
			out.print(delimiter); //",");
		}
		if (quote){
			out.print(escapeAndQuote(value));
		} else {
			out.print(value);
		}
		out.flush();
	}

	/**
	 * enclose the value in quotes and escape the quote
	 * and comma characters that are inside.
	 *
	 * @param value needs to be escaped and quoted
	 * @return the value, escaped and quoted.
	 */
	private static String escapeAndQuote(String value){
		int count = 2;
		for (int i=0; i<value.length(); i++){
			switch(value.charAt(i)){
				case '\"': case '\n': case '\r': case '\\': {
					count ++;
				} break;
			}
		}
		StringBuffer sb = new StringBuffer(value.length() + count);
		sb.append('"');
		for (int i=0; i<value.length(); i++){
			char c = value.charAt(i);
			switch(c){
				case '\"': {
					/*sb.append("\\\"");*/
                                        sb.append("\"");
                                        sb.append('"');
				} break;
				case '\n': {
					sb.append("\\n");
				} break;
				case '\r': {
					sb.append("\\r");
				} break;
				case '\\': {
					sb.append("\\\\");
				} break;
				default: {
					sb.append(c);
				}
			}
		}
		sb.append('"');
		return (sb.toString());
	}

}
/
show errors java source "xxntFile"

set define on

EXIT;
