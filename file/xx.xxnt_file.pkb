CREATE OR REPLACE PACKAGE BODY xx.xxnt_file
 --
 -- $Id:  $
 --
AS
 --
 -- ============================================================================
 --
 -- Module Name    : xx.xxnt_file.pkb
 -- Original Author: M Proctor
 -- Date           : 17-April-2019
 -- Description    : Package body for file utilities java wrappers.
 --                  Do Not call this package directly.
 --                  Use class xxnt_file_o
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       17-Apr-2019   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PRIVATE METHODS
 -- +--------------------------------------------------------------------------+
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION to_boolean(p_boolean  IN  number)
 RETURN Boolean
 IS
 BEGIN
  --
  IF p_boolean = 0 THEN
   --
   RETURN FALSE;
   --
  ELSE
   --
   RETURN TRUE;
   --
  END IF;
  --
 END to_boolean;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE chk_file_id(p_file_id IN number)
 IS
 BEGIN
  --
  If p_file_id IS NULL Then
    --
    xxnt_message.raise_error('Not a valid file object.');
    --
  End If;
  --
 END chk_file_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PUBLIC METHODS
 -- +--------------------------------------------------------------------------+
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_separator
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getSeparator() return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION separator
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( get_separator() );
  --
 END separator;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_file_id(p_filename IN varchar2)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.getFileId(java.lang.String) return int';

 FUNCTION get_fileid(p_filename IN varchar2)
 RETURN number
 IS
 BEGIN
  --
  RETURN get_file_id(p_filename => p_filename );
  --
 END get_fileid;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION does_exist(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.exists(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION exist(p_file_id IN number)
 RETURN Boolean
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( does_exist(p_file_id => p_file_id )
             )
         );
  --
 END exist;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_name(p_file_id IN number)
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getName(int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION name(p_file_id IN number)
 RETURN varchar2
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_name( p_file_id => p_file_id ) );
  --
 END name;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_path(p_file_id IN number)
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getPath(int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION path(p_file_id IN number)
 RETURN varchar2
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_path( p_file_id => p_file_id ) );
  --
 END path;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_absolute_path(p_file_id IN number)
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getAbsolutePath(int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION absolute_path(p_file_id IN number)
 RETURN varchar2
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_absolute_path(p_file_id => p_file_id) );
  --
 END absolute_path;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_canonical_path(p_file_id IN number)
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getCanonicalPath(int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION canonical_path(p_file_id IN number)
 RETURN varchar2
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_canonical_path(p_file_id => p_file_id) );
  --
 END canonical_path;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_parent_path(p_file_id IN number)
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getParent(int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION parent_path(p_file_id IN number)
 RETURN varchar2
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_parent_path(p_file_id => p_file_id) );
  --
 END parent_path;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION can_be_read(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.canRead(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION can_read(p_file_id IN number)
 RETURN Boolean
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( can_be_read(p_file_id => p_file_id )
             )
         );
  --
 END can_read;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION can_be_written(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.canWrite(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION can_write(p_file_id IN number)
 RETURN Boolean
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( can_be_written(p_file_id => p_file_id )
             )
         );
  --
 END can_write;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION is_a_file(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.isFile(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION is_file(p_file_id IN number)
 RETURN Boolean
 AS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( is_a_file(p_file_id => p_file_id )
             )
         );
  --
 END is_file;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION is_a_directory(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.isDirectory(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION is_directory(p_file_id IN number)
 RETURN Boolean
 AS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( is_a_directory(p_file_id => p_file_id )
             )
         );
  --
 END is_directory;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION create_new_file(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.createNewFile(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION create_new(p_file_id IN number)
 RETURN Boolean
 AS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( create_new_file(p_file_id => p_file_id )
             )
         );
  --
 END create_new;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE copy_file( p_file_id      IN number
                     ,p_new_filename IN varchar2 )
 AS LANGUAGE JAVA NAME 'xxntFile.copy(int, java.lang.String)';
 -- ----------------------------------------------------------------------------
 PROCEDURE copy( p_file_id      IN number
                ,p_new_filename IN varchar2 )
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  copy_file( p_file_id      => p_file_id
            ,p_new_filename => p_new_filename );
  --
 END copy;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION rename_file_to( p_file_id      IN number
                         ,p_new_filename IN varchar2 )
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.renameTo(int, java.lang.String) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION rename_to( p_file_id      IN number
                    ,p_new_filename IN varchar2 )
 RETURN number
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( rename_file_to( p_file_id      => p_file_id
                          ,p_new_filename => p_new_filename )
         );
 --
 END rename_to;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION delete_a_file(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.delete(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION delete_file(p_file_id IN number)
 RETURN Boolean
 AS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( xxnt_file.to_boolean
             ( delete_a_file(p_file_id => p_file_id )
             )
         );
  --
 END delete_file;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION filecount(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.fileCount(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION file_count(p_file_id IN number)
 RETURN number
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  --
  If NOT is_directory(p_file_id => p_file_id) Then
    --
    xxnt_message.raise_error('Invalid operation: Not a directory.');
    --
  End If;
  --
  --
  --
  RETURN ( filecount(p_file_id => p_file_id) );
  --
 END file_count;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION dircount(p_file_id IN number)
 RETURN number
 AS LANGUAGE JAVA NAME 'xxntFile.dirCount(int) return int';
 -- ----------------------------------------------------------------------------
 FUNCTION directory_count(p_file_id IN number)
 RETURN number
 IS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  --
  If NOT is_directory(p_file_id => p_file_id) Then
    --
    xxnt_message.raise_error('Invalid operation: Not a directory.');
    --
  End If;
  --
  --
  --
  RETURN ( dircount(p_file_id => p_file_id) );
  --
 END directory_count;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION get_list_file_name( p_file_id IN number
                             ,p_index   IN number )
 RETURN varchar2
 AS LANGUAGE JAVA NAME 'xxntFile.getFileName(int, int) return java.lang.String';
 -- ----------------------------------------------------------------------------
 FUNCTION get_listfile_name( p_file_id IN number
                            ,p_index   IN number )
 RETURN varchar2
 AS
 BEGIN
  --
  chk_file_id(p_file_id => p_file_id );
  --
  RETURN ( get_list_file_name( p_file_id => p_file_id
                              ,p_index   => p_index )
         );
  --
 END get_listfile_name;
 --
 --
 --
 --
 --
END xxnt_file;
/
sho err package body xxnt_file

EXIT;

