--
set define off
--
CREATE OR REPLACE AND COMPILE JAVA SOURCE NAMED "xxntList" AS

import java.util.ArrayList;

 /*
 -- +==========================================================================+
 -- |                    National Trust
 -- +==========================================================================+
 -- | File Name   : xxntList.java
 -- | Module      : Utilities
 -- | Author      : M Proctor
 -- | Date        : 17-Apr-2019
 -- | Description : File Utilities
 -- |
 -- | Doc Ref(s)  : 
 -- |
 -- | Change Record:
 -- | ===============
 -- | Ver  Date         Author           Comments
 -- | ===  ===========  =============    ======================================+
 -- | 1.0  17-Apr-2019  M Proctor        Initial Build
 -- | 
 -- | 
 -- +==========================================================================+
*/


public class xxntList 
{

  private ArrayList list = new ArrayList();
  
  public int addElement(Object element)  
  {
    for (int i=0; i<list.size(); i++) 
    {
      if (list.get(i) == null) 
      {
        list.add(i,element);
        return i;
      }
    }

    list.add(element);
    return list.size()-1;
  }

  public void removeElement(int index) 
  {
    list.add(index, null);
  }

  public Object get(int index) 
  {
    return list.get(index);
  }
}
/ 
--show errors java source "xxntList"

EXIT;

