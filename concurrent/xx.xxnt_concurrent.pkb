CREATE OR REPLACE PACKAGE BODY xx.xxnt_concurrent
 --
 -- $Id:  $
 --
 AS
 --
 -- ============================================================================
 --
 -- Module Name    : xx.xxnt_concurrent.pkb
 -- Original Author: M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Package body for concurrent program utilities.
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PRIVATE PACKAGE GLOBALS
 -- +---------------------------------------------------------------------------
 --
 g_logging_on                           Boolean := FALSE;
 g_logging_level                        number  := 6;
 --
 --
 g_status_normal               CONSTANT number := 0;
 g_status_warning              CONSTANT number := 1;
 g_status_error                CONSTANT number := 2;
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PUBLIC METHODS
 -- +---------------------------------------------------------------------------
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Global Values
 -- +---------------------------------------------------------------------------
 --
 -- Completion Statuses
 --
 FUNCTION status_normal
 RETURN number
 IS
 BEGIN
  --
  RETURN ( g_status_normal );
  --
 END status_normal;
 --
 --
 FUNCTION status_warning
 RETURN number
 IS
 BEGIN
  --
  RETURN ( g_status_warning );
  --
 END status_warning;
 --
 --
 FUNCTION status_error
 RETURN number
 IS
 BEGIN
  --
  RETURN ( g_status_error );
  --
 END status_error;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION program_info
              ( p_conc_prog_id    IN number
               ,p_conc_request_id IN number )
 RETURN prog_info_rec
 IS
  --
  CURSOR cs_sel( cp_conc_prog_id    IN number
                ,cp_conc_request_id IN number )
  IS
  SELECT (SELECT fcp.concurrent_program_name
            FROM applsys.fnd_concurrent_programs fcp
           WHERE fcpt.concurrent_program_id
                      = fcp.concurrent_program_id)             program_name
        ,to_char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') start_date
        ,fcpt.user_concurrent_program_name                     user_program_name
        ,fcpt.description                                      description
        ,fcp.application_id                                    application_id
   FROM  applsys.fnd_concurrent_programs fcp
        ,applsys.fnd_concurrent_programs_tl fcpt
        ,applsys.fnd_concurrent_requests fcr
   WHERE fcp.concurrent_program_id = cp_conc_prog_id
     AND fcpt.concurrent_program_id = fcp.concurrent_program_id
     AND fcpt.language = userenv('LANG')
     AND fcr.concurrent_program_id = fcpt.concurrent_program_id
     AND fcr.request_id = cp_conc_request_id;
  --
  r_sel  cs_sel%ROWTYPE;
  --
  l_program_info  prog_info_rec;
  --
 BEGIN
  --
  OPEN  cs_sel( cp_conc_prog_id    => p_conc_prog_id
               ,cp_conc_request_id => p_conc_request_id );
  FETCH cs_sel INTO r_sel;
  CLOSE cs_sel;
  --
  --
  l_program_info.program_name      := r_sel.program_name     ;
  l_program_info.user_program_name := r_sel.user_program_name;
  l_program_info.description       := r_sel.description      ;
  l_program_info.application_id    := r_sel.application_id   ;
  l_program_info.actual_start_date := r_sel.start_date       ;
  --
  --
  --
  RETURN ( l_program_info );
  --
 END program_info;
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Logging
 -- +---------------------------------------------------------------------------
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_on
 IS
 BEGIN
  --
  g_logging_on := TRUE;
  --
 END set_logging_on;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_off
 IS
 BEGIN
  --
  g_logging_on := FALSE;
  --
 END set_logging_off;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION logging_on
 RETURN Boolean
 IS
 BEGIN
  --
  RETURN ( g_logging_on );
  --
 END logging_on;
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_level
              ( p_level IN number )
 IS
 BEGIN
  --
  g_logging_level := p_level;
  --
  If g_logging_level > 0 Then
    --
    set_logging_on();
    --
  End If;
  --
 END set_logging_level;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION logging_level
 RETURN number
 IS
 BEGIN
  --
  RETURN ( g_logging_level );
  --
 END logging_level;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE log_save
 IS
 BEGIN
  --
  xxnt_log.save();
  --
 END log_save;
 --
 --
 --
 --
 --
END xxnt_concurrent;
/
sho err package body xx.xxnt_concurrent

EXIT;
