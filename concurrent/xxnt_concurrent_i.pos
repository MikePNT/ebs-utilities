CREATE OR REPLACE TYPE xxnt_concurrent_i FORCE
 --
 /* $Id: $ */
 --
 AUTHID CURRENT_USER
 --
 UNDER xx.xxnt_object_a
 --
 -- ============================================================================
 --
 -- Name           : xxnt_concurrent_i.pos
 -- Author         : M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Abstract class specification for concurrent program
 --                  utility.
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 ( 
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Global Values
 -- +---------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< Completion Statuses
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Return the appropriate numeric value indicated by the function names.
 --
 --
 -- ----------------------------------------------------------------------------
 --
 --
 NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION status_normal
 RETURN number
 --
 --
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION status_warning
 RETURN number
 --
 --
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION status_error
 RETURN number
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Active Program Globals
 -- +---------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< request_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent request id.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION request_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent program id.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION program_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent program name.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION program_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent user program name.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION user_program_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_description
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent program description.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION program_description
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_application_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent program application id.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION program_application_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< actual_start_date
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session concurrent program actual start date/time.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION actual_start_date
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session user id.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION user_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session user name.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION user_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session responsibility id.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION responsibility_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Session responsibility name.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION responsibility_name
 RETURN varchar2
 --
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_header
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Writes a standard concurrent program header to the concurrent log using
 --  the provided logger.
 --  This will list the user/translated program name, description, user name,
 --  responsibility name, request id, actual start date/time, and a list of all
 --  enabled parameters by their Prompt Name, along with the actual runtime
 --  values of the parameters.
 --
 -- Sample log:
 --=============================================================================
 --]Begin Processing: NT: Campaign Load
 --]Description     : NT: Campaign Load
 --] 
 --]User   : MPROCTOR
 --]Resp   : NT Data Quality
 --]Request: 119875174
 --]Start  : 11-APR-2019 11:29:08
 --] 
 --]Parameters:
 --]  Directory Name                : XX_CAMPAIGN_LOAD_DIR
 --]  WIP File Name                 : CampaignLoadWIP.csv
 --]  Logging Level                 : 3
 --]  Child program parameter2      : 
 --]  Child program parameter3      : 
 --]  Child program parameter4      : 
 --]  Child program parameter5      : 
 --] 
 --]============================================================================
 --
 --
 -- Parameters:
 -- Name                In/Out   Datatype        Description
 --  p_logger            IN       xxnt_logger_i   Logger of the calling unit.
 --  p_to_log_only       IN       Boolean         By default the report is
 --                                               printed to log and output.
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE write_header
              ( p_logger      IN xxnt_logger_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Writes a standard concurrent program footer to the concurrent log using
 --  the provided logger.
 --  Will list the summary activity report.
 --  This will include counts of; records select for processing, errors,
 --  successful record creation, successful updates, records ignored / skipped,
 --  deletes and elapsed time.
 --
 -- Sample log:
 --]============================================================================
 --]Process Results:
 --]
 --]Processed : 14638 records selected for processing.
 --]There were: 35 errors.
 --]There were: 0 successfully created.
 --]There were: 0 successfully updated.
 --]Ignored   : 14603 records were ignored as unnecessary updates.
 --]There were: 0 records were deleted.
 --]
 --]Completion Date/Time: 11-APR-2019 08:47
 --]Elapsed time        : 00:00:35.82
 --]
 --]============================================================================
 --
 --
 -- Parameters:
 -- Name                In/Out   Datatype        Description
 --  p_logger            IN       xxnt_logger_i   Logger of the calling unit.
 --  p_to_log_only       IN       Boolean         By default the report is
 --                                               printed to log and output.
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE write_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_counter     IN xxnt_dml_counter_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_abbreviated_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Writes a reduced concurrent program footer to the concurrent log using
 --  the provided logger.
 --
 -- Sample log:
 --]============================================================================
 --]Processing: NT: Campaign Load - V2 completed.
 --]Completion Date/Time: 15-APR-2019 09:24:37
 --]
 --]Processed    : 1 files.
 --]
 --]Elapsed time        : 00:00:33.28
 --]
 --]================================================================================
 --
 --
 -- Parameters:
 -- Name                In/Out   Datatype        Description
 --  p_logger            IN       xxnt_logger_i   Logger of the calling unit.
 --  p_to_log_only       IN       Boolean         By default the report is
 --                                               printed to log and output.
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE write_abbreviated_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_message     IN varchar2 DEFAULT NULL
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Logging
 -- +---------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Set logging to on.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE set_logging_on
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_off
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Set logging to off.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE set_logging_off
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Returns Boolean TRUE if logging is on.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION logging_on
 RETURN Boolean
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Sets the logging level of concurrent processes.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE set_logging_level
              ( p_level IN number )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Returns the logging level as a numeric value.
 --
 --
 -- ----------------------------------------------------------------------------
 ,NOT INSTANTIABLE NOT FINAL
 MEMBER FUNCTION logging_level
 RETURN number
 --
 --
 --
 --
 --
) NOT INSTANTIABLE NOT FINAL
/
sho err type xxnt_concurrent_i

EXIT;
