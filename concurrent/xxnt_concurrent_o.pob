CREATE OR REPLACE TYPE BODY xxnt_concurrent_o
 --
 -- $Id:  $
 --
 AS
 --
 -- ============================================================================
 --
 -- Module Name    : xxnt_concurrent_o.pob
 -- Original Author: M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Type body for concurrent program utilities.
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Override and disable the default constructor.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_concurrent_o
                   ( class                IN xx.xxnt_class_o
                    ,prog_name            IN varchar2
                    ,user_prog_name       IN varchar2
                    ,prog_description     IN varchar2
                    ,prog_application_id  IN number
                    ,prog_start_date      IN varchar2
                    ,divider_str          IN varchar2
                   )
 RETURN SELF AS RESULT
 IS
 BEGIN
  --
  SELF.init_class();
  SELF.class.disabled_constructor_error();
  --
  --
  --
  RETURN;
  --
 END xxnt_concurrent_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Formal constructor
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_concurrent_o
 RETURN SELF AS RESULT
 IS
  --
  l_prog_info   xx.xxnt_concurrent.prog_info_rec;
  --
 BEGIN
  --
  SELF.init_class();
  --
  l_prog_info := xx.xxnt_concurrent.program_info
                        ( p_conc_prog_id    => SELF.program_id()
                         ,p_conc_request_id => SELF.request_id() );
  --
  --
  SELF.prog_name           := l_prog_info.program_name     ;
  SELF.user_prog_name      := l_prog_info.user_program_name;
  SELF.prog_description    := l_prog_info.description      ;
  SELF.prog_application_id := l_prog_info.application_id   ;
  SELF.prog_start_date     := l_prog_info.actual_start_date;
  --
  --
  --
  RETURN;
  --
 END xxnt_concurrent_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Formal constructor
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_concurrent_o
              ( p_level IN number )
 RETURN SELF AS RESULT
 IS
  --
  l_prog_info   xx.xxnt_concurrent.prog_info_rec;
  --
 BEGIN
  --
  SELF.init_class();
  SELF.set_logging_level( p_level => p_level );
  SELF.set_logging_on();
  --
  l_prog_info := xx.xxnt_concurrent.program_info
                        ( p_conc_prog_id    => SELF.program_id()
                         ,p_conc_request_id => SELF.request_id() );
  --
  SELF.prog_name           := l_prog_info.program_name     ;
  SELF.user_prog_name      := l_prog_info.user_program_name;
  SELF.prog_description    := l_prog_info.description      ;
  SELF.prog_application_id := l_prog_info.application_id   ;
  SELF.prog_start_date     := l_prog_info.actual_start_date;
  SELF.divider_str         := rpad('=', 80, '=')           ;
  --
  --
  --
  RETURN;
  --
 END xxnt_concurrent_o;
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Global Values
 -- +---------------------------------------------------------------------------
 --
 -- Completion Statuses
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_normal
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_normal
 RETURN number
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_concurrent.status_normal() );
  --
 END status_normal;
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_warning
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_warning
 RETURN number
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_concurrent.status_warning() );
  --
 END status_warning;
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_error
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_error
 RETURN number
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_concurrent.status_error() );
  --
 END status_error;
 --
 --
 --
 --
 -- +------------------------
 -- | Active Program Globals
 -- +------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< request_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION request_id
 RETURN number
 IS
 BEGIN
  --
  RETURN ( fnd_global.conc_request_id );
  --
 END request_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_id
 RETURN number
 IS
 BEGIN
  --
  RETURN ( fnd_global.conc_program_id );
  --
 END program_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_name
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.prog_name );
  --
 END program_name;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_program_name
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.user_prog_name );
  --
 END user_program_name;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_description
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_description
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.prog_description );
  --
 END program_description;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_application_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_application_id
 RETURN number
 IS
 BEGIN
  --
  RETURN ( SELF.prog_application_id );
  --
 END program_application_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< actual_start_date
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION actual_start_date
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.prog_start_date );
  --
 END actual_start_date;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_id
 RETURN number
 IS
 BEGIN
  --
  RETURN ( fnd_global.user_id );
  --
 END user_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_name
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( fnd_global.user_name );
  --
 END user_name;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION responsibility_id
 RETURN number
 IS
 BEGIN
  --
  RETURN ( fnd_global.resp_id );
  --
 END responsibility_id;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION responsibility_name
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( fnd_global.resp_name );
  --
 END responsibility_name;
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Standard Summaries
 -- +---------------------------------------------------------------------------
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_header
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_header
              ( p_logger      IN xxnt_logger_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 IS
  --
  l_logger            xxnt_logger_i := xxnt_logger_factory_o.create_logger();
  --
  --
  CURSOR cs_param( cp_application_id      IN number
                  ,cp_desc_flexfield_name IN varchar2 )
  IS
  SELECT fdfcu.column_seq_num
        ,fdfcu.end_user_column_name
        ,fdfcu.form_left_prompt
    FROM fnd_descr_flex_col_usage_vl fdfcu
   WHERE application_id = cp_application_id
     AND fdfcu.descriptive_flexfield_name = cp_desc_flexfield_name
     AND fdfcu.enabled_flag = 'Y'
   ORDER BY fdfcu.column_seq_num;
  --
  --
  l_rep_logger        xxnt_logger_i := p_logger;
  l_desc_flex_name    varchar2(70)  := '$SRS$.'||SELF.program_name();
  l_application_id    number        := SELF.program_application_id();
  l_request_id        number        := SELF.request_id();
  l_sql               varchar2(32767);
  l_value             varchar2(32767);
  l_ctr               number        := 0;
  --
  --
  -- -----------------------------------
  PROCEDURE do_log(p_message IN varchar2)
  IS
  BEGIN
   If NOT p_to_log_only Then
     l_rep_logger.info_output(p_message);
   Else
     l_rep_logger.info(p_message);
   End If;
  END do_log;
   --
 BEGIN
  --
  l_logger.entering();
  --
  --
  do_log(' ');
  do_log(SELF.divider_str);
  do_log('Begin Processing: '||SELF.user_program_name());
  do_log('Description     : '||SELF.program_description());
  do_log(' ');
  do_log('User   : '||SELF.user_name());
  do_log('Resp   : '||SELF.responsibility_name());
  do_log('Request: '||SELF.request_id());
  do_log('Start  : '||SELF.actual_start_date());
  do_log(' ');
  do_log('Parameters:');
  --
  --
  --
  l_logger.set_location(10);
  FOR r_param IN cs_param( cp_application_id      => l_application_id
                          ,cp_desc_flexfield_name => l_desc_flex_name )
   LOOP
    --
    l_ctr := l_ctr + 1;
    --
    If l_ctr <= 25 Then
      --
      l_sql :=  'DECLARE '                                ||chr(10)||
                ' l_value    varchar2(1000); '            ||chr(10)||
                ' BEGIN '                                 ||chr(10)||
                ' SELECT argument'||l_ctr                 ||chr(10)||
                ' INTO :l_value'                          ||chr(10)||
                ' FROM fnd_concurrent_requests'           ||chr(10)||
                ' WHERE request_id = '||l_request_id||'; '||chr(10)||
                'END;';
      --
    ElsIf l_ctr >= 26 AND
          l_ctr <= 100 Then
      --
      l_sql :=  'DECLARE '                                ||chr(10)||
                ' l_value    varchar2(1000); '            ||chr(10)||
                ' BEGIN '                                 ||chr(10)||
                ' SELECT argument'||l_ctr                 ||chr(10)||
                ' INTO :l_value'                          ||chr(10)||
                ' FROM fnd_conc_request_arguments'        ||chr(10)||
                ' WHERE request_id = '||l_request_id||'; '||chr(10)||
                'END;';
      --
    End If;
    --
    --
    l_logger.debug('l_sql: '||l_sql);
    --
    --
    l_logger.set_location(20);
    EXECUTE IMMEDIATE l_sql USING OUT l_value;
    --
    --
  --do_log('  '||rpad(r_param.end_user_column_name, 30)||': '||l_value);
    do_log('  '||rpad(r_param.form_left_prompt, 30)||': '||l_value);
    --
  END LOOP;
  --
  --
  l_logger.set_location(30);
  do_log(' ');
  do_log(SELF.divider_str);
  do_log(' ');
  --
  --
  --
  l_logger.leaving();
  --
 END write_header;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_counter     IN xxnt_dml_counter_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 IS
  --
  l_logger            xxnt_logger_i := xxnt_logger_factory_o.create_logger();
  --
  --
  l_rep_logger        xxnt_logger_i := p_logger;
  l_timer             xxnt_timer_i  := p_timer;
  l_processed         number        := 0;
  --
  --
  -- -----------------------------------
  PROCEDURE do_log(p_message IN varchar2)
  IS
  BEGIN
   If NOT p_to_log_only Then
     l_rep_logger.info_output(p_message);
   Else
     l_rep_logger.info(p_message);
   End If;
  END do_log;
   --
 BEGIN
  --
  l_logger.entering();
  --
  --
  l_processed := nvl(p_counter.delete_count(), 0) + 
                 nvl(p_counter.select_count(), 0);
  --
  --
  --
  do_log(' ');
  do_log(' ');
  do_log(SELF.divider_str);
  do_log('Process Results:');
  do_log(' ');
  do_log('Processed : ' ||l_processed||' records selected '||
                    'for processing.');
  do_log('There were: ' ||p_counter.error_count()  ||' errors.');
  do_log('There were: ' ||p_counter.insert_count() ||' successfully '||
                'created. ');
  do_log('There were: ' ||p_counter.update_count() ||' successfully '||
                    'updated. ');
  do_log('Ignored   : ' ||p_counter.skipped_count()||' records were '||
                'ignored as unnecessary updates.');
  do_log('There were: ' ||p_counter.delete_count() ||' records '||
                    'deleted.');
  do_log(' ');
  do_log('Completion Date/Time: '||
                    to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
  do_log('Elapsed time        : '||l_timer.formatted_elapsed() );
  do_log(' ');
  --
  do_log(SELF.divider_str);
  do_log(' ');
  --
  --
  --
  l_logger.leaving();
  --
 END write_footer;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_abbreviated_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_abbreviated_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_message     IN varchar2 DEFAULT NULL
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 IS
  --
  l_logger            xxnt_logger_i := xxnt_logger_factory_o.create_logger();
  --
  --
  l_rep_logger        xxnt_logger_i := p_logger;
  l_timer             xxnt_timer_i  := p_timer;
  --
  --
  -- -----------------------------------
  PROCEDURE do_log(p_message IN varchar2)
  IS
  BEGIN
   If NOT p_to_log_only Then
     l_rep_logger.info_output(p_message);
   Else
     l_rep_logger.info(p_message);
   End If;
  END do_log;
   --
 BEGIN
  --
  l_logger.entering();
  --
  --
  do_log(' ');
  do_log(SELF.divider_str);
  do_log('Processing: '||SELF.user_program_name() ||' completed.');
  do_log('Completion Date/Time: '||
                to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
  do_log('Elapsed time        : '||l_timer.formatted_elapsed() );
  do_log(' ');
  --
  If p_message IS NOT NULL Then
    --
    do_log(p_message);
    do_log(' ');
    --
  End If;
  --
  do_log(' ');
  do_log(SELF.divider_str);
  do_log(' ');
  --
  --
  --
  l_logger.leaving();
  --
 END write_abbreviated_footer;
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Logging
 -- +---------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_on
 IS
 BEGIN
  --
  xx.xxnt_concurrent.set_logging_on();
  --
 END set_logging_on;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_off
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_off
 IS
 BEGIN
  --
  xx.xxnt_concurrent.set_logging_off();
  --
 END set_logging_off;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION logging_on
 RETURN Boolean
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_concurrent.logging_on() );
  --
 END logging_on;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_level
              ( p_level IN number )
 IS
 BEGIN
  --
  xx.xxnt_concurrent.set_logging_level
              (p_level => p_level );
  --
 END set_logging_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION logging_level
 RETURN number
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_concurrent.logging_level() );
  --
 END logging_level;
 --
 --
 --
 --
 --
END;
/
sho err type body xxnt_concurrent_o

EXIT;
