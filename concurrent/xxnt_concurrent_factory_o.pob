CREATE OR REPLACE TYPE BODY xxnt_concurrent_factory_o
 --
 /* $Id:  $ */
 --
 AS
 --
 -- ============================================================================
 --
 -- Name           : xxnt_concurrent_factory_o.pob
 -- Author         : M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Static class body for concurrent object creation.
 --
 --
 --
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_concurrent_factory_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Override and disable the default constructor.
 --  This class is intenionally not Instantiable (static).
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_concurrent_factory_o
                   ( class             IN xx.xxnt_class_o
                   )
 RETURN SELF AS RESULT
 IS
 BEGIN
  --
  SELF.init_class();
  xxnt_message.raise_error('Default constructor '''||
                           SELF.get_class_name()||''' is disabled.');
  --
  --
  --
  RETURN;
  --
 END xxnt_concurrent_factory_o;
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< create_concurrent
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 STATIC FUNCTION create_concurrent
               ( p_logging_level     IN number
               )
 RETURN xxnt_concurrent_i
 IS
  --
  l_level    number := nvl(p_logging_level, 3);
  --
 BEGIN
  --
  -- Minimum detail allowed. Must be 3 or less.
  -- 3 is standard log reporting.
  --
  If l_level > 3 Then
    --
    l_level := 3;
    --
  End If;
  --
  --
  --
  RETURN ( xxnt_concurrent_o
             ( p_level => l_level
             )
         );
  --
 END create_concurrent;
 --
 --
 --
 --
 --
END;
/
sho err type body xxnt_concurrent_factory_o

EXIT;

