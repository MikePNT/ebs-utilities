CREATE OR REPLACE TYPE xxnt_concurrent_o
 --
 /* $Id: $ */
 --
 AUTHID CURRENT_USER
 --
 UNDER xxnt_concurrent_i
 --
 -- ============================================================================
 --
 -- Name           : xxnt_concurrent_o.pos
 -- Author         : M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Type specification for concurrent program utilities.
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 ( prog_name                     varchar2(30)
  ,user_prog_name                varchar2(240)
  ,prog_description              varchar2(240)
  ,prog_application_id           number
  ,prog_start_date               varchar2(30)
  ,divider_str                   varchar2(200)
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Override and disable the default constructor.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 ,CONSTRUCTOR FUNCTION xxnt_concurrent_o
                   ( SELF                 IN OUT xxnt_concurrent_o
                    ,class                IN xx.xxnt_class_o
                    ,prog_name            IN varchar2
                    ,user_prog_name       IN varchar2
                    ,prog_description     IN varchar2
                    ,prog_application_id  IN number
                    ,prog_start_date      IN varchar2
                    ,divider_str          IN varchar2
                   )
 RETURN SELF AS RESULT
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Formal constructor
 --  Caller must explicitly set the logging level (set_logging_level).
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 ,CONSTRUCTOR FUNCTION xxnt_concurrent_o
 RETURN SELF AS RESULT
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< xxnt_concurrent_o
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  Formal constructor
 --  - Sets the desired logging level
 --  - Turns logging on for the session.
 --
 --
 -- Parameters:
 --  Name                 Type            Description
 --   p_level              number          The desired logging level
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 ,CONSTRUCTOR FUNCTION xxnt_concurrent_o
              ( p_level IN number )
 RETURN SELF AS RESULT
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Global Values
 -- +---------------------------------------------------------------------------
 --
 -- Completion Statuses
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_normal
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_normal
 RETURN number
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_warning
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_warning
 RETURN number
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< status_error
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION status_error
 RETURN number
 --
 --
 --
 --
 -- +------------------------
 -- | Active Program Globals
 -- +------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< request_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION request_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_program_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_program_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_description
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_description
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< program_application_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION program_application_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< actual_start_date
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION actual_start_date
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< user_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION user_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_id
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION responsibility_id
 RETURN number
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< responsibility_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION responsibility_name
 RETURN varchar2
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Standard Summaries
 -- +---------------------------------------------------------------------------
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_header
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_header
              ( p_logger      IN xxnt_logger_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_counter     IN xxnt_dml_counter_i
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< write_mini_footer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE write_abbreviated_footer
              ( p_logger      IN xxnt_logger_i
               ,p_timer       IN xxnt_timer_i
               ,p_message     IN varchar2 DEFAULT NULL
               ,p_to_log_only IN Boolean DEFAULT FALSE
              )
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Logging
 -- +---------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_on
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_off
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_off
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION logging_on
 RETURN Boolean
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER PROCEDURE set_logging_level
              ( p_level IN number )
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See supertype class spcification.
 --
 --
 -- ----------------------------------------------------------------------------
 ,OVERRIDING
 INSTANTIABLE FINAL
 MEMBER FUNCTION logging_level
 RETURN number
 --
 --
 --
 --
 --
) INSTANTIABLE FINAL
/
sho err type xxnt_concurrent_o

EXIT;
