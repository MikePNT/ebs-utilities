CREATE OR REPLACE PACKAGE xx.xxnt_concurrent
 --
 -- $Id:  $
 --
 AUTHID CURRENT_USER
 --
 AS
 --
 -- ============================================================================
 --
 -- Module Name    : xx.xxnt_concurrent.pkh
 -- Original Author: M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Package specification for concurrent program utilities.
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- Version   Date          Author            Description
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PUBLIC PACKAGE GLOBALS and TYPES
 -- +---------------------------------------------------------------------------
 --
 TYPE prog_info_rec IS RECORD
 ( program_name       applsys.fnd_concurrent_programs.concurrent_program_name%TYPE
  ,user_program_name  applsys.fnd_concurrent_programs_tl.user_concurrent_program_name%TYPE
  ,description        applsys.fnd_concurrent_programs_tl.description%TYPE
  ,application_id     number
  ,actual_start_date  varchar2(30)
 );
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PUBLIC METHODS
 -- +---------------------------------------------------------------------------
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Global Values
 -- +---------------------------------------------------------------------------
 --
 -- Completion Statuses
 -- -------------------
 --
 FUNCTION status_normal
 RETURN number;
 --
 --
 FUNCTION status_warning
 RETURN number;
 --
 --
 FUNCTION status_error
 RETURN number;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION program_info
              ( p_conc_prog_id    IN number
               ,p_conc_request_id IN number )
 RETURN prog_info_rec;
 --
 --
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | Logging
 -- +---------------------------------------------------------------------------
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_on;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_off;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION logging_on
 RETURN Boolean;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_logging_level
              ( p_level IN number );
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION logging_level
 RETURN number;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE log_save;
 --
 --
 --
 --
 --
END xxnt_concurrent;
/
sho err package xx.xxnt_concurrent

EXIT;
