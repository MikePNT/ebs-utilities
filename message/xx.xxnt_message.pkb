CREATE OR REPLACE PACKAGE BODY xx.xxnt_message
 --
 /* $Id:  $ */
 --
 AS
 --
 -- ----------------------------------------------------------------------------
 --
 -- Name       : xx.xxnt_message.pkb
 -- Author     : M Proctor
 -- Date       : 14-Feb-2019
 -- Description: Message package body.
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       14-Feb-2019   M Proctor         Initial version
 --
 --
 --
 --
 --g_message_number    number :=  -20100;
 -- x+x+n+t=82
 g_message_number    number :=  -20082;
 --
 --
 -- ----------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_message
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_message
            ( p_message_name   IN varchar2
             ,p_app_short_name IN varchar2 DEFAULT 'XX' )
 IS
 BEGIN
  --
  apps.fnd_message.set_name(p_app_short_name, p_message_name);
  --
 END set_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_token
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_token
            ( p_token_name     IN varchar2
             ,p_token          IN varchar2 )
 IS
 BEGIN
  --
  apps.fnd_message.set_token(p_token_name, p_token);
  --
 END set_token;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_text
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_text
            ( p_message_text   IN varchar2 )
 IS
 BEGIN
  --
  apps.fnd_message.set_name('XX','NTZZ_ERROR_STACK_MSG');
  apps.fnd_message.set_token('MSG_TEXT',p_message_text);
  --
 END set_text;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< raise_error
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE raise_error
 IS
  --
  l_message  varchar2(32767);
  --
 BEGIN
  --
  If xxnt_log.log_level <= xxnt_log.g_level_procedure Then
    --
    l_message := apps.fnd_message.get();
    --
  Else
    --
    l_message := xxnt_log.current_location ||' : '||apps.fnd_message.get();
    --
  End if;
  --
  --
  raise_application_error(g_message_number, l_message );
  --apps.fnd_message.raise_error();
  --
 END raise_error;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< raise_error
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE raise_error
            ( p_message_text   IN varchar2 )
 IS
 BEGIN
  --
  set_text(p_message_text);
  raise_error();
  --
 END raise_error;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< current_message
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION current_message
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( apps.fnd_message.get() );
  --
 END current_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< clear
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE clear
 IS
 BEGIN
  --
  apps.fnd_message.clear();
  --
 END clear;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< if_not_success_raise
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE if_not_success_raise
            ( p_status      IN varchar2
             ,p_msg_count   IN number
             ,p_msg_data    IN varchar2
            )
 IS
  --
  l_status        varchar2(30) := nvl(p_status, 'NULL');
  l_msg_count     number := nvl(p_msg_count, 0);
  l_part_message  varchar2(32767);
  l_final_message varchar2(2000);
  --
 BEGIN
  --
  If l_status <> apps.fnd_api.g_ret_sts_success Then
    --
    If l_msg_count > 0 Then
      --
      FOR i IN 1..l_msg_count
       LOOP
        --
        l_part_message := apps.fnd_msg_pub.get
                           ( p_msg_index => i
                            ,p_encoded   => apps.fnd_api.g_false ) || chr(10);
        --
        l_final_message := substr(trim( l_final_message ||' '||
                                        l_part_message), 1, 2000 );
        --
      END LOOP;
      --
      --
      raise_error
            ( p_message_text => l_final_message );
      --
    End If;
    --
  End If;
  --
  --
 END if_not_success_raise;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2 )
 IS
  l_logger  xxnt_logger_i := xxnt_logger_factory_o.create_logger();
 BEGIN
  --
  l_logger.critical('Error: '||p_sqlcode);
  l_logger.critical('Error: '||p_sqlerrm);
  l_logger.critical(DBMS_UTILITY.format_error_backtrace);
  --
 END when_others;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others_concsub
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others_concsub
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2
             ,p_status  OUT varchar2
             ,p_message OUT varchar2 )
 IS
 BEGIN
  --
  when_others
      ( p_sqlcode => p_sqlcode
       ,p_sqlerrm => p_sqlerrm );
  --
  --
  p_status  := xx.xxnt_concurrent.status_error();
  p_message := 'Error: '||p_sqlerrm;
  --
 END when_others_concsub;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others_raise
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others_raise
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2 )
 IS
 BEGIN
  --
  when_others
      ( p_sqlcode => p_sqlcode
       ,p_sqlerrm => p_sqlerrm );
  --
  --
  raise_error
      ( p_message_text => p_sqlerrm );
  --
 END when_others_raise;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< java_sql_exception
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION java_sql_exception
            ( p_message IN varchar2 )
 RETURN varchar2
 IS
  --
  l_message        varchar2(32767) := p_message;
  l_keyword        varchar2(10)    := 'java.sql.';
  l_orakey         varchar2(10)    := 'ORA-';
  l_null_pointer   varchar2(30)    := 'java.lang.NullPointerException';
  l_keyword_len  pls_integer       := lengthb(l_keyword);
  l_keyword_loc  pls_integer;
  --
 BEGIN
  --
  If l_message IS NOT NULL Then
    --
    If instr(l_message, l_null_pointer) = 0 Then
      --
      l_keyword_loc := instr(l_message, l_keyword);
      If l_keyword_loc != 0 Then
        --
        l_message := substr(l_message, l_keyword_loc);
        l_keyword_loc := instr(l_message, l_orakey);
        l_message := substr(l_message, l_keyword_loc);
        --
      End If;
      --
    End If;
    --
  End If;
  --
  --
  --
  RETURN ( l_message );
  --
 END java_sql_exception;
 --
 --
 --
 --
 --
END xxnt_message;
/
sho err package body xx.xxnt_message

EXIT;
