CREATE OR REPLACE PACKAGE BODY xxnt_message
 --
 /* $Id:  $ */
 --
 AS
 --
 -- ----------------------------------------------------------------------------
 --
 -- Name       : xxnt_message.pkb
 -- Author     : M Proctor
 -- Date       : 14-Feb-2019
 -- Description: Message package body.
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       14-Feb-2019   M Proctor         Initial version
 --
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_message
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_message
            ( p_message_name   IN varchar2
             ,p_app_short_name IN varchar2 DEFAULT 'XX' )
 IS
 BEGIN
  --
  xx.xxnt_message.set_message
            ( p_message_name   => p_message_name
             ,p_app_short_name => p_app_short_name );
  --
 END set_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_token
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_token
            ( p_token_name     IN varchar2
             ,p_token          IN varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.set_token
            ( p_token_name => p_token_name
             ,p_token      => p_token );
  --
 END set_token;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_text
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_text
            ( p_message_text   IN varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.set_text
            ( p_message_text => p_message_text );
  --
 END set_text;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< raise_error
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE raise_error
 IS
 BEGIN
  --
  xx.xxnt_message.raise_error();
  --
 END raise_error;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< raise_error
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE raise_error
            ( p_message_text   IN varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.raise_error
            ( p_message_text => p_message_text );
  --
 END raise_error;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< current_message
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION current_message
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( xx.xxnt_message.current_message() );
  --
 END current_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< clear
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE clear
 IS
 BEGIN
  --
  xx.xxnt_message.clear();
  --
 END clear;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< if_not_success_raise
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE if_not_success_raise
            ( p_status      IN varchar2
             ,p_msg_count   IN number
             ,p_msg_data    IN varchar2
            )
 IS
  --
 BEGIN
  --
  xx.xxnt_message.if_not_success_raise
            ( p_status     => p_status
             ,p_msg_count  => p_msg_count
             ,p_msg_data   => p_msg_data );
  --
 END if_not_success_raise;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.when_others
            ( p_sqlcode => p_sqlcode
             ,p_sqlerrm => p_sqlerrm );
  --
 END when_others;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others_concsub
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others_concsub
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2
             ,p_status  OUT varchar2
             ,p_message OUT varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.when_others_concsub
      ( p_sqlcode => p_sqlcode
       ,p_sqlerrm => p_sqlerrm
       ,p_status  => p_status
       ,p_message => p_message
      );
  --
  --
 END when_others_concsub;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< when_others_raise
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE when_others_raise
            ( p_sqlcode IN number
             ,p_sqlerrm IN varchar2 )
 IS
 BEGIN
  --
  xx.xxnt_message.when_others_raise
            ( p_sqlcode => p_sqlcode
             ,p_sqlerrm => p_sqlerrm );
  --
 END when_others_raise;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< java_sql_exception
 -- +--------------------------------------------------------------------------+
 --  Description:
 --   See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION java_sql_exception
            ( p_message IN varchar2 )
 RETURN varchar2
 IS
  --
 BEGIN
  --
  --
  --
  RETURN ( xx.xxnt_message.java_sql_exception
            ( p_message => p_message )
         );
  --
 END java_sql_exception;
 --
 --
 --
 --
 --
END xxnt_message;
/
sho err package body xxnt_message

EXIT;
