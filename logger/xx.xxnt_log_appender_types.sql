 -- ============================================================================
 --
 -- MODULE NAME    : xx.xxnt_log_appender_types.sql
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Build XXNT table objects
 --
 --
 -- Dependencies:
 -- -------------
 -- None
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
DECLARE
 --
 CURSOR cs_sel
 IS
 SELECT null
   FROM user_objects
  WHERE object_name = 'XXNT_LOG_APPENDER_TYPES';
 --
 r_sel  cs_sel%ROWTYPE;
 --
BEGIN
 --
 OPEN  cs_sel;
 FETCH cs_sel INTO r_sel;
 If cs_sel%found Then
   EXECUTE IMMEDIATE 'DROP TABLE xx.xxnt_log_appender_types';
 End If;
 CLOSE cs_sel;
 --
END;
/



CREATE TABLE xx.xxnt_log_appender_types
( log_type_code                     varchar2(30)
 ,appender_type_name                varchar2(61)
 ,status_type_name                  varchar2(61)
 ,default_layout_name               varchar2(61)
 ,layout_max_message_length         number
 ,enabled_flag                      varchar2(1)
-- ,set_method                        varchar2(100)
-- ,get_method                        varchar2(100)
 ,created_by                        number
 ,creation_date                     date
 ,last_update_date                  date
 ,last_updated_by                   number
 ,last_update_login                 number
 ,CONSTRAINT xxnt_log_appender_types_pk PRIMARY KEY (log_type_code)
  USING INDEX TABLESPACE apps_ts_tx_idx
 ,CONSTRAINT xxnt_log_appender_types_nn01 CHECK (appender_type_name IS NOT NULL)
 ,CONSTRAINT xxnt_log_appender_types_nn02 CHECK (status_type_name IS NOT NULL)
 ,CONSTRAINT xxnt_log_appender_types_nn03 CHECK (default_layout_name IS NOT NULL)
 ,CONSTRAINT xxnt_log_appender_types_nn04 CHECK (layout_max_message_length IS NOT NULL)
 ,CONSTRAINT xxnt_log_appender_types_nn05 CHECK (enabled_flag IS NOT NULL)
) TABLESPACE xx
/

EXIT;

