CREATE OR REPLACE PACKAGE BODY xx.xxnt_logger_factory
 --
 /* $Id:  $ */
 --
 AS
 --
 -- ============================================================================
 --
 --
 -- MODULE NAME    : xx.xxnt_logger_factory.pkb
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 08-Jul-2019
 -- Description    : Package body for logger creation
 --
 --
 --
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       08-Jul-2019   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PRIVATE TYPES AND GLOBALS
 -- +--------------------------------------------------------------------------+
 --
 --
 --
 TYPE logger_list IS TABLE OF xx.xxnt_logger_a;
 --
 --
 --
 g_loggers      logger_list := logger_list();
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PRIVATE METHODS
 -- +--------------------------------------------------------------------------+
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< create_logger_list
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Creates a list of loggers for quick selection
 --
 --  All indexes +1 to allow for 0 (zero) logging level vs first index of 1.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE create_logger_list
 IS
  --
  l_unnamed   varchar2(30) := 'Unnamed.logger';
  --
 BEGIN
  --
  g_loggers.EXTEND(xxnt_log.g_level_xx_mandatory+1);
  --
  --
  g_loggers(xxnt_log.g_level_off+1)         := xxnt_logger_unexpected_o
                                              ( p_name => l_unnamed
                                              );
  --
  g_loggers(xxnt_log.g_level_statement+1)   := xxnt_logger_statement_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_procedure+1)   := xxnt_logger_procedure_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_event+1)       := xxnt_logger_event_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_exception+1)   := xxnt_logger_exception_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_error+1)       := xxnt_logger_error_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_unexpected+1)   := xxnt_logger_unexpected_o
                                              ( p_name => l_unnamed
                                              );
  g_loggers(xxnt_log.g_level_xx_mandatory+1) := xxnt_logger_mandatory_o
                                              ( p_name => l_unnamed
                                              );
  --
  --
 END create_logger_list;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PUBLIC METHODS
 -- +--------------------------------------------------------------------------+
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< create_logger
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 --  All indexes +1 to allow for 0 (zero) logging level vs first index of 1.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION create_logger
               ( p_method_name       IN varchar2
               )
 RETURN xxnt_logger_i
 IS
 BEGIN
  --
  g_loggers(xxnt_log.log_level()+1).name := p_method_name;
  --
  --
  --
  RETURN ( g_loggers(xxnt_log.log_level()+1) );
  --
 END create_logger;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< create_logger
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Public factory method.
 --  Returns a log object for use.
 --
 -- Parameters:
 --  Name                  Data Type   Description
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION create_logger
 RETURN xxnt_logger_i
 IS
  --
  l_proc        varchar2(61) := 'xxnt_logger_factory_o.create_logger-3';
  --
  l_method      varchar2(1000);
  l_level       number := 3; -- sub-program level
  --
 BEGIN
  --
  l_method := lower(utl_call_stack.concatenate_subprogram
                                         (utl_call_stack.subprogram(l_level)));
  --
  --
  --
  RETURN ( create_logger
             ( p_method_name => l_method
             )
         );
  --
 END create_logger;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PACKAGE INSTANTIATION
 -- +--------------------------------------------------------------------------+
 --
 BEGIN
 --
 create_logger_list();
 --
 --
 --
 --
 --
END xxnt_logger_factory;
/
sho err package body xx.xxnt_logger_factory

EXIT;
