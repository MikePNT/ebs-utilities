CREATE OR REPLACE TYPE BODY xx.xxnt_log_message_large_o
 --
 -- $Id:  $
 --
 AS
 --
 -- ============================================================================
 --
 --
 -- MODULE NAME    : xx.xxnt_log_message_large_o.pob
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- Description    : Class body for xxnt_log_message_large_o tracing utility.
 --
 --
 --
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_log_message_large_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Override and disable the default constructor.
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_log_message_large_o
                   ( class             IN xx.xxnt_class_o
                    ,logger_name       IN varchar2
                    ,date_time         IN date
                    ,elapsed_time      IN number
                    ,message_level     IN number
                    ,level_name        IN varchar2
                    ,content_type      IN varchar2
                    ,message_lines     IN xx.xxnt_strings_201_t
                   )
 RETURN SELF AS RESULT
 IS
 BEGIN
  --
  SELF.init_class();
  SELF.class.disabled_constructor_error();
  --
  --
  --
  RETURN;
  --
 END xxnt_log_message_large_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_log_message_large_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Formal constructor method.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_log_message_large_o
                   ( p_logger_name       IN varchar2
                    ,p_date_time         IN date
                    ,p_elapsed_time      IN number
                    ,p_message_level     IN number
                    ,p_level_name        IN varchar2
                    ,p_content_type      IN varchar2
                    ,p_message           IN varchar2
                   )
 RETURN SELF AS RESULT
 IS
  --
  l_length              number;
  l_max_message_length  number  := 200; -- 1 less than xxnt_strings_201_t
  l_position            number  := l_max_message_length;
  l_continue            Boolean := TRUE;
  --
 BEGIN
  --
  SELF.init_class();
  SELF.logger_name       := p_logger_name          ;
  SELF.date_time         := p_date_time            ;
  SELF.elapsed_time      := p_elapsed_time         ;
  SELF.message_level     := p_message_level        ;
  SELF.level_name        := p_level_name           ;
  SELF.content_type      := p_content_type         ;
  SELF.message_lines     := xxnt_strings_201_t()   ;
  --
  l_length               := lengthb(p_message);
  --
  WHILE l_continue
   LOOP
    --
    SELF.message_lines.EXTEND();
        SELF.message_lines(SELF.message_lines.LAST())
             := substrb( p_message
                        ,(l_position - l_max_message_length) + 1
                        ,l_max_message_length);
    --
    l_continue := (l_length > l_position);
    l_position := l_position + l_max_message_length;
    --
  END LOOP;
  --
  --
  --
  RETURN;
  --
 END xxnt_log_message_large_o;
 --
 --
 --
 --
 --
-- -- +--------------------------------------------------------------------------+
-- -- |--< get_logger_name
-- -- +--------------------------------------------------------------------------+
-- --
-- -- Description:
-- --  Returns the logger name.
-- --
-- --
-- --
-- -- ----------------------------------------------------------------------------
-- OVERRIDING
-- MEMBER FUNCTION get_logger_name
-- RETURN varchar2
-- IS
-- BEGIN
--  --
--  RETURN ( SELF.logger_name );
--  --
-- END get_logger_name;
 --
 --
 --
 --
 --
-- -- +--------------------------------------------------------------------------+
-- -- |--< get_date_time
-- -- +--------------------------------------------------------------------------+
-- --
-- -- Description:
-- --  Returns the date and time.
-- --
-- --
-- --
-- -- ----------------------------------------------------------------------------
-- OVERRIDING
-- MEMBER FUNCTION get_date_time
-- RETURN date
-- IS
-- BEGIN
--  --
--  RETURN ( SELF.date_time );
--  --
-- END get_date_time;
 --
 --
 --
 --
 --
-- -- +--------------------------------------------------------------------------+
-- -- |--< get_elapsed_time
-- -- +--------------------------------------------------------------------------+
-- --
-- -- Description:
-- --  Returns the elapsed time.
-- --
-- --
-- --
-- -- ----------------------------------------------------------------------------
-- OVERRIDING
-- MEMBER FUNCTION get_elapsed_time
-- RETURN number
-- IS
-- BEGIN
--  --
--  RETURN ( SELF.elapsed_time );
--  --
-- END get_elapsed_time;
 --
 --
 --
 --
 --
-- -- +--------------------------------------------------------------------------+
-- -- |--< get_message_level
-- -- +--------------------------------------------------------------------------+
-- --
-- -- Description:
-- --  Returns the log level.
-- --
-- --
-- --
-- -- ----------------------------------------------------------------------------
-- OVERRIDING
-- MEMBER FUNCTION get_message_level
-- RETURN number
-- IS
-- BEGIN
--  --
--  RETURN ( SELF.message_level );
--  --
-- END get_message_level;
 --
 --
 --
 --
 --
-- -- +--------------------------------------------------------------------------+
-- -- |--< get_level_name
-- -- +--------------------------------------------------------------------------+
-- --
-- -- Description:
-- --  Returns the message level name.
-- --
-- --
-- --
-- -- ----------------------------------------------------------------------------
-- OVERRIDING
-- MEMBER FUNCTION get_level_name
-- RETURN varchar2
-- IS
-- BEGIN
--  --
--  RETURN ( SELF.level_name );
--  --
-- END get_level_name;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< get_logger_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Returns the message text.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION get_message_text
 RETURN varchar2
 IS
  --
  l_message_text  varchar2(32767);
  --
 BEGIN
  --
  FOR i IN SELF.message_lines.FIRST() .. SELF.message_lines.LAST()
   LOOP
    --
    l_message_text := l_message_text||SELF.message_lines(i);
    --
  END LOOP;
  --
  --
  --
  RETURN ( l_message_text );
  --
 END get_message_text;
 --
 --
 --
 --
 --
END;
/
sho err type body xxnt_log_message_large_o

EXIT;
