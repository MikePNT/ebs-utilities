 -- ============================================================================
 --
 -- MODULE NAME    : xx.populate_appender_types.sql
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Populate XXNT table objects
 --
 --
 -- Dependencies:
 -- -------------
 -- None
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --

INSERT INTO xx.xxnt_log_appender_types
 ( log_type_code
  ,appender_type_name
  ,status_type_name
  ,default_layout_name
  ,layout_max_message_length
  ,enabled_flag
  ,created_by
  ,creation_date
  ,last_update_date
  ,last_updated_by
  ,last_update_login
 )
VALUES
 ( 'FND_LOG'
  ,'xx.xxnt_log_appender_fnd_log_o'
  ,'xx.xxnt_log_status_fnd_log_o'
  ,'xx.xxnt_log_layout_default_o'
  ,4000
  ,'Y'
  ,1
  ,sysdate
  ,sysdate
  ,1
  ,-1
)
/

INSERT INTO xx.xxnt_log_appender_types
 ( log_type_code
  ,appender_type_name
  ,status_type_name
  ,default_layout_name
  ,layout_max_message_length
  ,enabled_flag
  ,created_by
  ,creation_date
  ,last_update_date
  ,last_updated_by
  ,last_update_login
 )
VALUES
 ( 'FND_FILE'
  ,'xx.xxnt_log_appender_fnd_file_o'
  ,'xx.xxnt_log_status_fnd_file_o'
  ,'xx.xxnt_log_layout_default_o'
  ,4000
  ,'Y'
  ,1
  ,sysdate
  ,sysdate
  ,1
  ,-1
)
/

INSERT INTO xx.xxnt_log_appender_types
 ( log_type_code
  ,appender_type_name
  ,status_type_name
  ,default_layout_name
  ,layout_max_message_length
  ,enabled_flag
  ,created_by
  ,creation_date
  ,last_update_date
  ,last_updated_by
  ,last_update_login
 )
VALUES
 ( 'FND_OUTPUT'
  ,'xx.xxnt_log_appender_fnd_output_o'
  ,'xx.xxnt_log_status_fnd_output_o'
  ,'xx.xxnt_log_layout_output_o'
  ,4000
  ,'Y'
  ,1
  ,sysdate
  ,sysdate
  ,1
  ,-1
 );

EXIT;

