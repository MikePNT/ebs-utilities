CREATE OR REPLACE PACKAGE BODY xx.xxnt_log_object_creator
 --
 AS
 --
 -- ============================================================================
 --
 -- Name           : xx.xxnt_log_object_creator.pkb
 -- Author         : M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : See package specification.
 --
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION log_status
                   ( p_log_type          IN varchar2
                   )
 RETURN xx.xxnt_log_status_i
 IS
  --
  CURSOR cs_sel(cp_log_type IN varchar2)
  IS
  SELECT log_type_code
        ,appender_type_name
        ,status_type_name
    FROM xx.xxnt_log_appender_types
   WHERE log_type_code = cp_log_type
     AND enabled_flag = 'Y';
  --
  r_sel  cs_sel%ROWTYPE;
  --
  --
  l_log_status     xx.xxnt_log_status_i;
  l_sql            varchar2(32767);
  --
 BEGIN
  --
  If p_log_type IS NOT NULL Then
    --
    OPEN  cs_sel(cp_log_type => p_log_type);
    FETCH cs_sel INTO r_sel;
    If cs_sel%notfound Then
      --
      xxnt_message.raise_error('Log Type '''||p_log_type||
                               ''' to ''create_log_status'' is '||
                               'not a valid registered log type.');
      --
    End If;
    CLOSE cs_sel;
    --
    l_sql := 'DECLARE '                                          ||chr(10)||
             ' l_log_status  xx.xxnt_log_status_i; '             ||chr(10)||
             'BEGIN '                                            ||chr(10)||
             ' :l_log_status := '||r_sel.status_type_name||'(); '||chr(10)||
             'END;';
    --
    --
    EXECUTE IMMEDIATE l_sql USING IN OUT l_log_status;
    --
    --
  Else
    --
    xxnt_message.raise_error('Log Type '''||p_log_type||
                             ''' to ''create_log_status'' may not '||
                             'be null.');
    --
  End If;
  --
  --
  --
  RETURN ( l_log_status );
  --
 END log_status;
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION log_appender
       	           ( p_log_type          IN varchar2
                    ,p_layout            IN xxnt_log_layout_i
                   )
 RETURN xx.xxnt_log_appender_i
 IS
  --
  CURSOR cs_sel(cp_log_type IN varchar2)
  IS
  SELECT log_type_code
        ,appender_type_name
        ,status_type_name
        ,default_layout_name
        ,layout_max_message_length
    FROM xx.xxnt_log_appender_types
   WHERE log_type_code = cp_log_type
     AND enabled_flag = 'Y';
  --
  r_sel  cs_sel%ROWTYPE;
  --
  --
  l_log_appender   xx.xxnt_log_appender_i;
  l_layout         xx.xxnt_log_layout_i;
  l_sql            varchar2(32767);
  --
 BEGIN
  --
  If p_log_type IS NOT NULL Then
    --
    OPEN  cs_sel(cp_log_type => p_log_type);
    FETCH cs_sel INTO r_sel;
    If cs_sel%notfound Then
      --
      xxnt_message.raise_error('Log Type ''p_log_type'||
                               ''' to ''create_log_appender'' is '||
                               'not a valid registered log type.');
      --
    End If;
    CLOSE cs_sel;
    --
    --
    --
    If p_layout IS NOT NULL Then
      --
      l_layout := p_layout;
      --
    Else
      --
      l_sql := 'DECLARE '                                           ||chr(10)||
              ' l_layout  xx.xxnt_log_layout_i; '                   ||chr(10)||
              'BEGIN '                                              ||chr(10)||
              ' :l_layout := '||r_sel.default_layout_name           ||chr(10)||
              '           ('||r_sel.layout_max_message_length||'); '||chr(10)||
              'END;';
      --
      --
      EXECUTE IMMEDIATE l_sql USING IN OUT l_layout;
      --
      --
    End If;
    --
    --
    --
    l_sql := 'DECLARE '                                          ||chr(10)||
             ' l_log_appender  xx.xxnt_log_appender_i; '         ||chr(10)||
             'BEGIN '                                            ||chr(10)||
             ' :l_log_appender := '||r_sel.appender_type_name    ||chr(10)||
             '                         (p_layout => :l_layout); '||chr(10)||
             'END;';
    --
    --
    EXECUTE IMMEDIATE l_sql USING IN OUT l_log_appender, IN l_layout;
    --
    --
    --
  Else
    --
    xxnt_message.raise_error('Log Type '''||p_log_type||
                             ''' to ''create_log_appender'' may not '||
                             'be null.');
    --
  End If;
  --
  --
  --
  RETURN ( l_log_appender );
  --
 END log_appender;
 --
 --
 --
 --
 --
END xxnt_log_object_creator;
/
sho err package xx.xxnt_log_object_creator

EXIT;
