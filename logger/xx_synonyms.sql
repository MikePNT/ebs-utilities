 -- ============================================================================
 --
 -- MODULE NAME    : xx_synonyms.sql
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Build XXNT objects - synonyms - Run as APPS
 --
 --
 -- Dependencies:
 -- -------------
 -- None
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       30-Jun-2019   M Proctor         Initial version
 -- 1.1       15-Aug-2019   M Proctor         Add dynamic drop for re-install
 -- 1.2       03-Sep-2019   M Proctor         Add synonyms for tables:
 --                                            fnd_profile_options
 --                                            fnd_profile_option_values
 --
 --
 --
 -- ============================================================================
 --
 --

DECLARE
 --
 CURSOR cs_sel
 IS
 SELECT 'DROP '||dbo.object_type||' '||dbo.owner||'.'||dbo.object_name drop_sql
   FROM dba_objects dbo
  WHERE dbo.owner = 'XX'
    AND dbo.object_type = 'SYNONYM'
    AND dbo.object_name IN ( 'FND_LOG_MESSAGES', 'FND_LOG_MESSAGES_S'
                            ,'FND_PROFILE_OPTIONS', 'FND_PROFILE_OPTION_VALUES'
                            ,'FND_LOG_TRANSACTION_CONTEXT' );
 --
 r_sel  cs_sel%ROWTYPE;
 --
BEGIN
 --
 FOR r_sel IN  cs_sel
  LOOP
   --
   EXECUTE IMMEDIATE r_sel.drop_sql;
   --
 END LOOP;
 --
END;
/


PROMPT Create synonym xx.fnd_log_messages
CREATE SYNONYM xx.fnd_log_messages FOR applsys.fnd_log_messages;

PROMPT Create synonym xx.fnd_log_messages_s
CREATE SYNONYM xx.fnd_log_messages_s FOR apps.fnd_log_messages_s;

PROMPT Create synonym xx.fnd_log_transaction_context
CREATE SYNONYM xx.fnd_log_transaction_context FOR applsys.fnd_log_transaction_context;

PROMPT Create synonym xx.fnd_profile_options
CREATE SYNONYM xx.fnd_profile_options FOR applsys.fnd_profile_options;

PROMPT Create synonym xx.fnd_profile_option_values
CREATE SYNONYM xx.fnd_profile_option_values FOR applsys.fnd_profile_option_values;



EXIT;
