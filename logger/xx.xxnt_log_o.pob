CREATE OR REPLACE TYPE BODY xx.xxnt_log_o
 --
 -- $Id:  $
 --
 AS
 --
 -- ============================================================================
 --
 -- Name           : xx.xxnt_log_o.pob
 -- Author         : M Proctor
 -- Date           : 12-Jan-2016
 -- Description    : Class body for xxnt_log_o tracing utility.
 --
 --
 --
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_log_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Override and disable the default constructor.
 --
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_log_o
                   ( class               IN xx.xxnt_class_o
                    ,min_log_level       IN number
                    ,max_log_level       IN number
                    ,log_level           IN number
                    ,log_on              IN varchar2
                    ,location            IN varchar2
                    ,timer_start         IN number
                    ,max_buffer          IN number
                    ,log_statuses        IN xx.xxnt_log_statuses_t
                    ,messages            IN xx.xxnt_log_messages_t
                    ,appenders           IN xx.xxnt_log_appenders_t
                    ,private_log_type    IN varchar2
                   )
 RETURN SELF AS RESULT
 IS
 BEGIN
  --
  SELF.init_class();
  SELF.class.disabled_constructor_error();
  --
  --
  --
  RETURN;
  --
 END xxnt_log_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_log_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Formal constructor method.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_log_o
                   ( p_log_level        IN varchar2
                    ,p_location         IN varchar2
                    ,p_max_buffer       IN number
                    ,p_private_log_type IN varchar2
                   )
 RETURN SELF AS RESULT
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.xxnt_log_o';
  --
  CURSOR cs_appends
  IS
  SELECT log_type_code
        ,appender_type_name
    FROM xx.xxnt_log_appender_types
   WHERE enabled_flag = 'Y';
  --
  --
  l_idx      number := 0;
  l_status   xx.xxnt_log_status_i;
  --l_layout   xxnt_log_layout_i := xxnt_log_layout_default_o(4000);
  --
 BEGIN
  --
  SELF.init_class();
  SELF.min_log_level       := apps.fnd_log.level_statement;
  SELF.max_log_level       := apps.fnd_log.level_unexpected;
  SELF.log_level           := nvl(p_log_level, SELF.max_log_level);
  SELF.location            := p_location;
  SELF.timer_start         := DBMS_UTILITY.get_time();
  SELF.max_buffer          := p_max_buffer;
  SELF.log_statuses        := xx.xxnt_log_statuses_t();
  SELF.messages            := xx.xxnt_log_messages_t();
  SELF.appenders           := xx.xxnt_log_appenders_t();
  SELF.private_log_type    := p_private_log_type;
  --
  --
  --
  -- Ultimately, these values 'FND_FILE', FND_LOG' (and any others) should be
  -- stored in a value set so the list of valid log types would be looped
  -- through using SQL, or initialised and stored in a table type attribute.
  --
  self.put_log(l_proc, 'Creating FND_FILE Status');
  --
  FOR r_appends IN cs_appends
   LOOP
    --
    SELF.log_statuses.EXTEND();
    l_idx := SELF.log_statuses.LAST();
    --
    SELF.log_statuses(l_idx) := xxnt_log_status_factory_o.create_log_status
                                 ( p_log_type    => r_appends.log_type_code
                                 );
    self.put_log(l_proc, r_appends.log_type_code||': '||
                         SELF.log_statuses(l_idx).get_log_level);
    --
    --
    -- check / set log level
    --
    If SELF.log_statuses(l_idx).get_log_level() < SELF.log_level AND
       SELF.log_statuses(l_idx).get_log_level() > 0 Then
      --
      SELF.log_level := SELF.log_statuses(l_idx).get_log_level();
      self.put_log(l_proc, 'log_level: '||SELF.log_level);
      --
    End If;
    --
    --
    -- Add matching appenders
    --
    self.put_log(l_proc, 'Adding Appenders');
    SELF.appenders.EXTEND();
    l_idx := SELF.appenders.LAST();
    SELF.appenders(l_idx) := xxnt_log_appender_factory_o.create_log_appender
                                 ( p_log_type  => r_appends.log_type_code );
    --
  END LOOP;
  --
  --
  --
  -- Set log status
  --
  If SELF.log_statuses.COUNT() > 0 Then
    --
    FOR i IN SELF.log_statuses.FIRST()..SELF.log_statuses.LAST()
     LOOP
      --
      If SELF.log_statuses(i).logging_on() Then
        --
        SELF.log_on := 'Y';
        EXIT;
        --
      End If;
      --
    END LOOP;
    --
    If SELF.log_on IS NULL Then
      --
      SELF.log_on := 'N';
      --
    End If;
    --
  End If;
  --
  --
  --
  self.put_log(l_proc, 'SELF.log_level: '||SELF.log_level);
  self.put_log(l_proc, 'SELF.log_on:    '||SELF.log_on);
  --
  self.put_log(l_proc, 'Leaving');
  RETURN;
  --
 END xxnt_log_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< xxnt_log_o
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 CONSTRUCTOR FUNCTION xxnt_log_o
                   ( p_private_log_type IN varchar2 DEFAULT NULL
                   )
 RETURN SELF AS RESULT
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.xxnt_log_o';
  --
  l_max_buffer number := 1; -- 3000
  --
 BEGIN
  --
  self.put_log(l_proc, 'Entering');
  --
  SELF := xxnt_log_o
           ( p_log_level        => 6
            ,p_location         => NULL
            ,p_max_buffer       => l_max_buffer
            ,p_private_log_type => p_private_log_type 
--            ,p_private_log_type => 'FND_FILE'
           );
  --
  --
  --
  self.put_log(l_proc, 'Leaving');
  RETURN;
  --
 END xxnt_log_o;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_log_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER PROCEDURE set_log_level
            ( p_level    IN number
            )
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.set_log_level';
  --
 BEGIN
  --
  SELF.log_level := p_level;
  --
 END set_log_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< get_log_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION get_log_level
            ( p_log_type IN varchar2 )
 RETURN number
 IS
  --
  l_proc        varchar2(61) := 'xxnt_log_o.get_log_level';
  --
  l_log_level   number := 6;
  --
 BEGIN
  --
  self.put_log(l_proc, 'Entering');
  --
  FOR i IN SELF.log_statuses.FIRST()..SELF.log_statuses.LAST()
   LOOP
    --
    If SELF.log_statuses(i).get_log_type() = p_log_type Then
      --
      l_log_level := SELF.log_statuses(i).get_log_level();
      self.put_log(l_proc, 'LOG Type: '||SELF.log_statuses(i).get_log_type());
      self.put_log(l_proc, 'LOG Level: '||SELF.log_statuses(i).get_log_level());
      --
    End If;
    --
  END LOOP;
  --
  self.put_log(l_proc, 'l_log_level: '||l_log_level);
  self.put_log(l_proc, 'SELF.log_level :'||SELF.log_level);
  --
  --
  self.put_log(l_proc, 'Leaving');
  RETURN ( l_log_level );
  --
 END get_log_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< get_log_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION get_log_level
 RETURN number
 IS
  --
  l_proc        varchar2(61) := 'xxnt_log_o.get_log_level';
  --
 BEGIN
  --
  self.put_log(l_proc, 'Entering');
  --
  self.put_log(l_proc, 'Leaving');
  RETURN ( SELF.log_level );
  --
 END get_log_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION logging_on
 RETURN Boolean
 IS
  --
  l_proc        varchar2(61) := 'xxnt_log_o.logging_on';
  --
 BEGIN
  --
  --
  --
  RETURN ( SELF.log_on = 'Y' );
  --
 END logging_on;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION logging_on
            ( p_log_type IN varchar2 )
 RETURN Boolean
 IS
  --
  l_proc        varchar2(61) := 'xxnt_log_o.logging_on';
  --
  l_logging_on  Boolean := FALSE;
  --
 BEGIN
  --
  If SELF.log_statuses.COUNT() > 0 Then
    --
    FOR i IN SELF.log_statuses.FIRST()..SELF.log_statuses.LAST()
     LOOP
      --
      If SELF.log_statuses(i).get_log_type() = p_log_type Then
        --
        l_logging_on := SELF.log_statuses(i).logging_on();
        EXIT;
        --
      End If;
      --
    END LOOP;
    --
  End If;
  --
  --
  RETURN ( l_logging_on );
  --
 END logging_on;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_location
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER PROCEDURE set_location
            ( p_location IN varchar2 )
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.set_location';
  --
 BEGIN
  --
  self.put_log(l_proc, 'Location: '||p_location);
  --
  SELF.location := p_location;
  --
 END set_location;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< get_current_location
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER FUNCTION get_current_location
 RETURN varchar2
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.get_current_location';
  --
 BEGIN
  --
  self.put_log(l_proc, 'Location: '||SELF.location);
  --
  RETURN ( SELF.location );
  --
 END get_current_location;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< add_message
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER PROCEDURE add_message
                   ( p_logger_name       IN varchar2
                    ,p_message_level     IN number
                    ,p_level_name        IN varchar2
                    ,p_content_type      IN varchar2
                    ,p_message           IN varchar2
                    ,p_message_count     OUT number
                   )
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.add_message';
  --
  l_logging_on    Boolean := SELF.logging_on();
  l_elapsed_time  number := DBMS_UTILITY.get_time() - SELF.timer_start;
  l_log_level     number := SELF.log_level;
  l_idx           number := 0;
  --
 BEGIN
  --
  self.put_log(l_proc, 'Entering');
  --
  --
  --
  self.put_log(l_proc, 'l_log_level: '||l_log_level);
  self.put_log(l_proc, 'p_message_level: '||p_message_level);
  --
  self.put_log(l_proc, 20);
  If SELF.logging_on() Then
    --
    self.put_log(l_proc, 30);
    If (p_message_level >= l_log_level) Then
      --
      self.put_log(l_proc, 40);
      SELF.messages.EXTEND();
      l_idx := SELF.messages.LAST();
      SELF.messages(l_idx) := xxnt_log_message_factory_o.create_log_message
                               ( p_logger_name   => p_logger_name
                                ,p_date_time     => sysdate
                                ,p_elapsed_time  => l_elapsed_time
                                ,p_message_level => p_message_level
                                ,p_level_name    => p_level_name
                                ,p_content_type  => p_content_type
                                ,p_message       => p_message
                               );
      --
    End If;
    --
  End If;
  --
  --
  --
  self.put_log(l_proc, 50);
  p_message_count := l_idx;
  If l_idx >= SELF.max_buffer then
    --
    self.put_log(l_proc, 60);
    SELF.save();
    --
  End If;
  --
  --
  --
  self.put_log(l_proc, 'Leaving');
  --
 END add_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< reset_timer
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER PROCEDURE reset_timer
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.reset_timer';
  --
 BEGIN
  --
  SELF.timer_start := DBMS_UTILITY.get_time();
  --
 END reset_timer;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< save
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 OVERRIDING
 MEMBER PROCEDURE save
 IS
  --
  l_proc     varchar2(61) := 'xxnt_log_o.save';
  --
  l_appender_level   number := 6;
  l_content_type     varchar2(30);
  --
 BEGIN
  --
  self.put_log(l_proc, 'Entering');
  --
  self.put_log(l_proc, 'message_count: '||SELF.messages.COUNT());
  If SELF.messages.COUNT() > 0 Then
    --
    self.put_log(l_proc, 'appenders_count: '||SELF.appenders.COUNT());
    If SELF.appenders.COUNT() > 0 Then
      --
      FOR i IN SELF.appenders.FIRST()..SELF.appenders.LAST()
       LOOP
         --
         l_appender_level := SELF.get_log_level(SELF.appenders(i).get_log_type());
         l_content_type   := SELF.appenders(i).get_content_type();
         --
         self.put_log(l_proc, 'get_log_type: '||
                      SELF.appenders(i).get_log_type());
         self.put_log(l_proc, 'l_appender_level: '||l_appender_level);
         --
         --
         If l_appender_level >= SELF.min_log_level Then
           --
           FOR j IN SELF.messages.FIRST()..SELF.messages.LAST()
            LOOP
             --
             self.put_log(l_proc, 'get_message_level:'||
                          SELF.messages(j).get_message_level());
             --
             If (SELF.messages(j).get_message_level() >= l_appender_level) AND
                (SELF.messages(j).get_content_type() = l_content_type) Then
               --
               self.put_log(l_proc, 'appending message: '||l_appender_level);
               --
               SELF.appenders(i).do_append(SELF.messages(j));
               --
             End If;
             --
           END LOOP;
           --
         End If;
         --
      END LOOP;
      --
    End If;
    --
  End If;
  --
  --
  --
  self.put_log(l_proc, 'Delete message buffer');
  SELF.messages.DELETE();
  --
  --
  --
  self.put_log(l_proc, 'Leaving');
  --
 END save;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< put_log
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See class specification.
 --
 --
 -- ----------------------------------------------------------------------------
 MEMBER PROCEDURE put_log
           ( SELF       IN xxnt_log_o
            ,p_location IN varchar2
            ,p_message  IN varchar2
           )
 IS
 BEGIN
  --
  If SELF.private_log_type IS NOT NULL Then
     --
    If SELF.private_log_type = 'FND_FILE' Then
      --
      apps.fnd_file.put_line( apps.fnd_file.log
                             ,p_location||': '||p_message);
      --
    ElsIf SELF.private_log_type = 'SCREEN' Then
      --
      DBMS_OUTPUT.put_line( p_location||': '||p_message);
      --
    End If;
    --
  End If;
  --
 END put_log;
 --
 --
 --
 --
 --
END;
/
sho err type body xx.xxnt_log_o

EXIT;
