CREATE OR REPLACE PACKAGE BODY xx.xxnt_log
 --
 -- $Id:  $
 --
 AS
 --
 -- ============================================================================
 --
 --
 -- MODULE NAME    : xx.xxnt_log.pkb
 -- ORIGINAL AUTHOR: M Proctor
 -- DATE           : 12-Jan-2016
 -- DESCRIPTION    : Package body for log / tracing utility.
 --
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 -- ============================================================================
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PRIVATE PACKAGE GLOBALS AND TYPES
 -- +---------------------------------------------------------------------------
 --
 g_log                      xxnt_log_i := xxnt_log_factory_o.create_log();
 --
 --
 TYPE level_name_map_t IS TABLE OF varchar2(30)
 INDEX BY binary_integer;
 --
 g_message_count            number           := 0;
 --
 g_level_name_map_list      level_name_map_t;
 g_max_level_name_len       number := 0;
 --
 --
 --
 -- +---------------------------------------------------------------------------
 -- | PUBLIC METHODS
 -- +---------------------------------------------------------------------------
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< init
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE init
 IS
  --
 BEGIN
  --
  g_log            := xxnt_log_factory_o.create_log();
  g_message_count  := 0;
  --
 END init;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_log_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_log_level
            ( p_level IN number )
 IS
  --
  l_level  integer := p_level;
  --
 BEGIN
  --
  If l_level IS NOT NULL Then
    --
    If l_level NOT between g_level_off AND g_level_xx_mandatory Then
      --
      xxnt_message.raise_error('Invalid log level: '||l_level||'. Valid ' ||
                               'levels are between '||g_level_off||' and '||
                               g_level_xx_mandatory||'.');
      --
    End If;
    --
  Else
   --
   xxnt_message.raise_error('Log level may not be set NULL.');
   --
  End If;
  --
  --
  --
  g_log.set_log_level
    ( p_level    => l_level
    );
  --
 END set_log_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< log_level
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION log_level
 RETURN number
 IS
  --
 BEGIN
  --
  RETURN ( g_log.get_log_level() );
  --
 END log_level;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< logging_on
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION logging_on
 RETURN Boolean
 IS
  --
 BEGIN
  --
  RETURN ( g_log.logging_on('FND_FILE') OR
           g_log.logging_on('FND_LOG') );
  --
 END logging_on;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_location_declare
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Sets a code location.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_location_declare
            ( p_method   IN varchar2 )
 IS
 BEGIN
  --
  g_log.set_location
            ( p_location => p_method||': '||g_declare );
  --
 END set_location_declare;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_location_entering
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Sets a code location.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_location_entering
            ( p_method   IN varchar2 )
 IS
 BEGIN
  --
  g_log.set_location
            ( p_location => p_method||': '||g_entering );
  --
 END set_location_entering;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_location_leaving
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Sets a code location.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_location_leaving
            ( p_method   IN varchar2 )
 IS
 BEGIN
  --
  g_log.set_location
            ( p_location => p_method||': '||g_leaving );
  --
 END set_location_leaving;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< set_location
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE set_location
            ( p_method   IN varchar2
             ,p_position IN varchar2 )
 IS
  --
 BEGIN
  --
  g_log.set_location
            (lower(p_method) ||': '||p_position);
  --
  --
 END set_location;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< add_message
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE add_message( p_logger_name   IN varchar2
                       ,p_message_level IN number
                       ,p_content_type  IN varchar2
                       ,p_message       IN varchar2 )
 IS
  --
 BEGIN
  --
  If g_log.logging_on() Then
    --
    If (p_message_level >= g_log.get_log_level) Then
      --
      g_log.add_message
             ( p_logger_name   => p_logger_name
              ,p_message_level => p_message_level
              ,p_level_name    => rpad( g_level_name_map_list(p_message_level)
                                       ,g_max_level_name_len)
              ,p_content_type  => p_content_type
              ,p_message       => p_message
              ,p_message_count => g_message_count
             );
      --
    End If;
    --
  End If;
  --
 END add_message;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< current_location
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION current_location
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( g_log.get_current_location() );
  --
 END current_location;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< save
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE save
 IS
 BEGIN
  --
  g_log.save();
  --
 END save;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< formatted_name
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  See package specification.
 --
 --
 --
 -- ----------------------------------------------------------------------------
 FUNCTION formatted_name
            ( p_name1  IN varchar2
             ,p_name2  IN varchar2 )
 RETURN varchar2
 IS
  --
  l_formatted   varchar2(100);
  --
 BEGIN
  --
  l_formatted := lower(trim(rtrim(trim(p_name1), '.')) ||
                 '.'                                   ||
                 trim(ltrim(trim(p_name2), '.')));
  --
  --
  set_location( p_method   => l_formatted
               ,p_position => g_declare );
  --
  --
  --
  RETURN ( l_formatted );
  --
 END formatted_name;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |--< debugger
 -- +--------------------------------------------------------------------------+
 --
 -- Description:
 --  Internal (logging) debug method.
 --  Do not use outside of xxnt logging utility.
 --
 --
 -- ----------------------------------------------------------------------------
 PROCEDURE debugger
            ( p_message IN varchar2 )
 IS
  --
  l_level       number := 3; -- sub-program level
  --
 BEGIN
  --
  null;
--  apps.fnd_file.put_line( apps.fnd_file.log
--                         ,lower(utl_call_stack.concatenate_subprogram
--                                         (utl_call_stack.subprogram(l_level)))||
--                         ': '||p_message );
  --
 END debugger;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- | PACKAGE INSTANTIATION
 -- +--------------------------------------------------------------------------+
 --
BEGIN
 --
-- debugger('xxnt_log: Instantiating Exec');
 --
 -- Map the names to the numbers.
 -- 
 --
-- g_level_name_map_list(0) := 'LEVEL_OFF'; -- null_logger no logging
-- g_level_name_map_list(1) := 'LEVEL_STATEMENT';
-- g_level_name_map_list(2) := 'LEVEL_PROCEDURE';
-- g_level_name_map_list(3) := 'LEVEL_EVENT';
-- g_level_name_map_list(4) := 'LEVEL_EXCEPTION';
-- g_level_name_map_list(5) := 'LEVEL_ERROR';
-- g_level_name_map_list(6) := 'LEVEL_UNEXPECTED';
-- g_level_name_map_list(7) := 'LEVEL_XX_MANDATORY';
 --
 g_level_name_map_list(g_level_off)          := 'Off'; -- null_logger / no logging
 g_level_name_map_list(g_level_statement)    := 'Statement';
 g_level_name_map_list(g_level_procedure)    := 'Procedure';
 g_level_name_map_list(g_level_event)        := 'Event';
 g_level_name_map_list(g_level_exception)    := 'Exception';
 g_level_name_map_list(g_level_error)        := 'Error';
 g_level_name_map_list(g_level_unexpected)   := 'Unexpected';
 g_level_name_map_list(g_level_xx_mandatory) := 'Mandatory';
 --
 --
 -- Find the longest string for formatting purposes
 --
 FOR i IN g_level_name_map_list.FIRST()..g_level_name_map_list.LAST()
  LOOP
   --
   If length(g_level_name_map_list(i)) > g_max_level_name_len Then
     --
     g_max_level_name_len := length(g_level_name_map_list(i));
     --
   End If;
   --
 END LOOP;
 --
 --
 --
END xxnt_log;
/
sho err package body xx.xxnt_log;

EXIT;
