CREATE OR REPLACE TYPE BODY xx.xxnt_object_a
 --
 /* $Id:  $ */
 --
 AS
 --
 -- ----------------------------------------------------------------------------
 --
 -- Name       : xx.xxnt_object_a.pob
 -- Author     : M Proctor
 -- Date       : 12-Jan-2016
 -- Description: Abstract class body for object.
 --
 --
 -- CHANGE HISTORY:
 --
 -- VERSION   DATE          AUTHOR            DESCRIPTION
 -- -------   -----------   ---------------   ----------------------------------
 -- 1.0       12-Jan-2016   M Proctor         Initial version
 --
 --
 --
 --
 --
 --
 -- ----------------------------------------------------------------------------
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< init_class
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE FINAL
 MEMBER PROCEDURE init_class
              ( p_class IN varchar2 )
 IS
 BEGIN
  --
  If p_class IS NULL Then
    --
    xxnt_message.raise_error('Internal error: Class name may not be set '||
                             'to null.');
    --
  End If;
  --
  --
  --
  SELF.class := xx.xxnt_class_o
              ( p_class => lower(trim(p_class)));
  --
  --
 END init_class;
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< init_class
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE FINAL
 MEMBER PROCEDURE init_class
 IS
  l_class_name  varchar2(2000);
 BEGIN
  --
  If SELF.class IS NULL Then
    --
    SELF.class := xx.xxnt_class_o(p_class => xx.xxnt_class_o.get_class_name
                                       (sys.AnyData.ConvertObject(SELF)));
    --
  End If;
  --
  --
 END init_class;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< get_class
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE FINAL
 MEMBER FUNCTION get_class
 RETURN xx.xxnt_class_o
 IS
 BEGIN
  --
  --If SELF.class IS NULL Then
  --  SELF.init_class();
  --End If;
  --
  RETURN ( SELF.class );
  --
 END get_class;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< get_class_name
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE FINAL
 MEMBER FUNCTION get_class_name
 RETURN varchar2
 IS
  l_class_name    varchar2(1000);
 BEGIN
  --
  -- The first option allows object constructors to use this method prior
  -- to calling the init_class() method.
  --
  If SELF.class IS NULL Then
    l_class_name := xx.xxnt_class_o.get_class_name
                            (p_class => sys.AnyData.ConvertObject(SELF));
--  Else
--    l_class_name := SELF.class.get_name();
    l_class_name := class.class;
  End If;
  --
  --
  If l_class_name IS NULL Then
    l_class_name := class.class;
  End if;
  --
  --
  RETURN ( lower(substr( l_class_name
                        ,instr(l_class_name, '.')+1)) );
--  RETURN ( xx.xxnt_class_o.get_class_name() );
  --
 END get_class_name;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< is_of_type
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE FINAL
 MEMBER FUNCTION is_of_type(p_obj IN xx.xxnt_object_a)
 RETURN Boolean
 IS
 BEGIN
  --
  RETURN ( p_obj IS OF (SELF) );
  --
 END is_of_type;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< map
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE NOT FINAL
 MAP MEMBER FUNCTION map
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.class.get_name() );
  --
 END map;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< equals
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE NOT FINAL
 MEMBER FUNCTION equals(p_obj IN xx.xxnt_object_a)
 RETURN Boolean
 IS
  l_equals   Boolean := FALSE;
 BEGIN
  --
  If SELF.is_of_type(p_obj => p_obj) AND
     SELF = p_obj  Then
    l_equals := TRUE;
  End If;
  --
  RETURN ( l_equals );
  --
 END equals;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< to_string
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE NOT FINAL
 MEMBER FUNCTION to_string
 RETURN varchar2
 IS
 BEGIN
  --
  RETURN ( SELF.class.get_name() );
  --
 END to_string;
 --
 --
 --
 --
 --
 --
 -- +--------------------------------------------------------------------------+
 -- |----< finalise
 -- +--------------------------------------------------------------------------+
 -- {Start of Comments}
 --
 -- Description:
 --  See type specification.
 --
 --
 -- {End of Comments}
 -- ----------------------------------------------------------------------------
 INSTANTIABLE NOT FINAL
 MEMBER PROCEDURE finalise
 IS
 BEGIN
  --
  NULL;
  --
 END finalise;
 --
 --
 --
 --
 --
END;
/
sho err type body xx.xxnt_object_a

EXIT;
